package ru.nsu.fit.g15205.uzlov.chat.common.core;

public class TagNamesConstants {
    public static final String ROOT_REQUEST = "command";
    public static final String ROOT_NOTIFICATION = "event";
    public static final String ROOT_SUCCESS = "success";
    public static final String ROOT_ERROR = "error";

    public static final String COMMAND_NAME_IDENTIFIER = "name";

    public static final String COMMAND_REQUEST_LOGIN = "login";
    public static final String COMMAND_REQUEST_LOGOUT = "logout";
    public static final String COMMAND_REQUEST_USER_LIST_REQUEST = "list";
    public static final String COMMAND_REQUEST_SEND_MESSAGE = "message";

    public static final String SUBTAG_NICKNAME = "name";
    public static final String SUBTAG_CLIENT_NAME = "type";

    public static final String SUBTAG_UID = "session";

    public static final String SUBTAG_ERROR_TEXT = "message";

    public static final String SUBTAG_USER_LIST = "listusers";
    public static final String SUBTAG_USER = "user";

    public static final String SUBTAG_MESSAGE_TEXT = "message";

    public static final String SUBTAG_TIME = "time";
    public static final String SUBTAG_FROM_ID = "fromID";
    public static final String tag = "";

    public static final String COMMAND_NOTIFICATION_NEW_MESSAGE = "message";
    public static final String COMMAND_NOTIFICATION_LOGIN = "userlogin";
    public static final String COMMAND_NOTIFICATION_LOGOUT = "userlogout";
}
