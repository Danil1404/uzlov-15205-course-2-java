package ru.nsu.fit.g15205.uzlov.chat.common.events;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import ru.nsu.fit.g15205.uzlov.chat.common.core.TagNamesConstants;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

/**
 * Created by Danil on 2017-07-15.
 */
public class SuccessAnswer extends Answer {
    private final String id;
    public SuccessAnswer() {
        this(null);
    }
    public SuccessAnswer(String id) {
        this.id = id;
    }
    @Override
    public Object getContent() {
        return id;
    }

    @Override
    protected Document generateDocument() {
        try {
            Document d = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
            Element s = d.createElement(TagNamesConstants.ROOT_SUCCESS);
            d.appendChild(s);

            if (id != null) {
                Element usid = d.createElement(TagNamesConstants.SUBTAG_UID);
                usid.setTextContent(id);
                s.appendChild(usid);
            }
            return d;
        } catch (ParserConfigurationException e) {
            throw new RuntimeException(e);
        }
    }
}
