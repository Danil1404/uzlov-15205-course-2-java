package ru.nsu.fit.g15205.uzlov.chat.common.events;

import org.w3c.dom.Document;

/**
 * Created by Danil on 2017-07-16.
 */
public class ConnectionError extends EventTemplate {
    @Override
    public Object getContent() {
        return null;
    }
    @Override
    protected Document generateDocument() {
        return null;
    }
}
