package ru.nsu.fit.g15205.uzlov.chat.common.events;

import ru.nsu.fit.g15205.uzlov.chat.common.core.Event;

/**
 * Superclass for events that are not independent and were created as acknowledgement or opposite.
 */
public abstract class Answer extends EventTemplate {
}
