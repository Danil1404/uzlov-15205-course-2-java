package ru.nsu.fit.g15205.uzlov.chat.server;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.nsu.fit.g15205.uzlov.chat.common.core.*;
import ru.nsu.fit.g15205.uzlov.chat.common.events.ConnectionError;
import ru.nsu.fit.g15205.uzlov.chat.common.events.ErrorAnswer;
import ru.nsu.fit.g15205.uzlov.chat.common.events.LoginRequest;
import ru.nsu.fit.g15205.uzlov.chat.common.network.SimpleSendTask;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Created by Danil on 2017-07-12.
 */
class LinkToUser {
    private static final Logger l = LogManager.getLogger();
    private final Transmitter transmitter;
    private final Thread receiverThread;
    private final Thread transmitterThread;
    private final Protocol protocol;
    private final EventHandler handler;
    private boolean isStopped = true;
    /**
     * user info is obtained from user with register request
     */
    private UserInfo info;
    LinkToUser(Protocol protocol, EventHandler incomingEventHandler) {
        this.protocol = protocol;
        this.handler = incomingEventHandler;
        this.transmitter = new Transmitter();

        this.receiverThread = new Thread(new Receiver());
        this.transmitterThread = new Thread(transmitter);
    }
    public void start() {
        l.debug("starting LinkToUser {}", this);
        isStopped = false;
        this.receiverThread.start();
        this.transmitterThread.start();
    }
    UserInfo getUserInfo() {
        return info;
    }
    void stop() {
        l.debug("stopping user {}", this);
        receiverThread.interrupt();
        transmitterThread.interrupt();
        protocol.close();
    }
    void send(SendTask task) {
        transmitter.send(task);
    }
    void send(Event event) {
        send(new SimpleSendTask(event));
    }
    public String getId() {
        return this.info.getUID();
    }
    public void setId(String id) {
        this.info.setUID(id);
    }

    private class Receiver implements Runnable {
        public void run() {
            l.debug("running Receiver (user {})", LinkToUser.this);
            Event e;
            try {
                e = protocol.getEvent();
            } catch (NetworkException | ParsingException e1) {
                l.error("user {}: receiver: can't read events: {}", LinkToUser.this, e1);
                send(new SimpleSendTask(new ErrorAnswer(e1.getMessage())));
                stop();
                return;
            }
            if (!(e instanceof LoginRequest)) {
                l.error("user {}: first event is NOT register request", LinkToUser.this);
                throw new RuntimeException("first message must be login request");
            }
            info = (UserInfo) e.getContent();
            handler.handleEvent(new ReceivedInfo(e, LinkToUser.this));

            l.debug("user {}: receiver: cycling for new events", LinkToUser.this);
            while (true) {
                try {
                    e = protocol.getEvent();
                } catch (NetworkException | ParsingException e1) {
                    l.error("user {}: receiver: can't read events: {}", LinkToUser.this, e1);
                    send(new SimpleSendTask(new ErrorAnswer(e1.getMessage())));
                    if (!isStopped) {
                        isStopped = true;
                        handler.handleEvent(new ReceivedInfo(new ConnectionError(), LinkToUser.this));
                    }
                    stop();
                    return;
                }
                if (e instanceof LoginRequest) {
                    l.error("user {}: receiver: login request after first event", LinkToUser.this);
                    send(new SimpleSendTask(new ErrorAnswer("second login request in single session")));
                    if (!isStopped) {
                        isStopped = true;
                        handler.handleEvent(new ReceivedInfo(new ConnectionError(), LinkToUser.this));
                    }
                    stop();
                    return;
                }
                handler.handleEvent(new ReceivedInfo(e, LinkToUser.this));
            }
        }
    }

    private class Transmitter implements Runnable {
        private final BlockingQueue<SendTask> transmitQueue = new LinkedBlockingQueue<>();
        void send(SendTask task) {
            transmitQueue.offer(task);
        }
        @Override
        public void run() {
            l.debug("running Transmitter (user {})", LinkToUser.this);
            try {
                while (true) {
                    SendTask task = transmitQueue.take();
                    l.debug("user {}: transmitter: sending next event", LinkToUser.this);
                    try {
                        protocol.send(task);
                    } catch (NetworkException e) {
                        l.error("user {}: transmitter: error sending event", LinkToUser.this);
                        if (!isStopped) {
                            isStopped = true;
                            handler.handleEvent(new ReceivedInfo(new ConnectionError(), LinkToUser.this));
                        }
                        stop();
                        return;
                    }
                }
            } catch (InterruptedException e) {
                l.debug("user {}: transmitter: interrupted", LinkToUser.this);
            }
        }
    }
}
