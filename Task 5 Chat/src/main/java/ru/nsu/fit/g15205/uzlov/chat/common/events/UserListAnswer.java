package ru.nsu.fit.g15205.uzlov.chat.common.events;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import ru.nsu.fit.g15205.uzlov.chat.common.core.TagNamesConstants;
import ru.nsu.fit.g15205.uzlov.chat.common.core.UserInfo;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.util.List;

/**
 * Created by Danil on 2017-07-15.
 */
public class UserListAnswer extends SuccessAnswer {
    private final List<UserInfo> userList;
    public UserListAnswer(List<UserInfo> userList) {
        assert userList != null;
        this.userList = userList;
    }
    @Override
    public List<UserInfo> getContent() {
        return userList;
    }
    @Override
    protected Document generateDocument() {
        try {
            Document d = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
            Element s = d.createElement(TagNamesConstants.ROOT_SUCCESS);
            d.appendChild(s);

            Element lu = d.createElement(TagNamesConstants.SUBTAG_USER_LIST);
            s.appendChild(lu);

            for (UserInfo info : userList) {
                Element u = d.createElement(TagNamesConstants.SUBTAG_USER);
                lu.appendChild(u);

                Element n = d.createElement(TagNamesConstants.SUBTAG_NICKNAME);
                n.setTextContent(info.getName());
                u.appendChild(n);

                Element t = d.createElement(TagNamesConstants.SUBTAG_CLIENT_NAME);
                t.setTextContent(info.getClient());
                u.appendChild(t);

                Element uid = d.createElement(TagNamesConstants.SUBTAG_UID);
                uid.setTextContent(info.getUID());
                u.appendChild(uid);
            }

            return d;
        } catch (ParserConfigurationException e) {
            throw new RuntimeException(e);
        }
    }
}
