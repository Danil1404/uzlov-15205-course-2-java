package ru.nsu.fit.g15205.uzlov.chat.client.GUI;

import ru.nsu.fit.g15205.uzlov.chat.common.core.VisibleMessage;
import ru.nsu.fit.g15205.uzlov.chat.common.messages.TextMessage;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Danil on 2017-07-12.
 */
public class MessagesListCellRenderer extends JPanel implements ListCellRenderer<VisibleMessage> {
    private final JLabel from;
    private final JLabel fromUID;
    private final JTextArea content;
    private final JLabel time;
    private final JPanel parent;
    MessagesListCellRenderer(JPanel parent) {
        this.setLayout(new BorderLayout());

        this.parent = parent;

        from = new JLabel();
        fromUID = new JLabel();
        content = new JTextArea();
        time = new JLabel();

        DialogueListCellRenderer.makeLookLikeLabel(content);


        JPanel fromPanel = new JPanel();
        fromPanel.add(from);
        fromPanel.add(new JLabel("#"));
        fromPanel.add(fromUID);

        JPanel messageHeader = new JPanel(new BorderLayout());
        messageHeader.add(fromPanel, BorderLayout.WEST);
        messageHeader.add(time, BorderLayout.EAST);

        this.add(messageHeader, BorderLayout.NORTH);
        this.add(content, BorderLayout.CENTER);

        //this.setBorder(BorderFactory.createLineBorder(Color.BLACK));
    }
    @Override
    public Component getListCellRendererComponent(JList<? extends VisibleMessage> list, VisibleMessage value, int index, boolean isSelected, boolean cellHasFocus) {

        if (value instanceof TextMessage) {
            this.setBorder(BorderFactory.createLineBorder(Color.BLACK));
            this.setBackground(list.getBackground());
        } else {
            this.setBorder(BorderFactory.createEmptyBorder());
            this.setBackground(list.getSelectionBackground());
        }
        this.setForeground(list.getForeground());

        from.setText(value.getFrom());
        fromUID.setText(value.getFromID());
        content.setText(value.getContent());
        time.setText(value.getTime().toString());

        content.setSize(parent.getSize().width, 1);
        //content.setSize(content.getPreferredSize());

        return this;
    }
}
