package ru.nsu.fit.g15205.uzlov.chat.common.core;

import java.time.Instant;

/**
 * Created by Danil on 2017-07-16.
 */
public interface VisibleMessage {
    String getContent();
    String getFrom();
    String getFromID();
    Instant getTime();
}
