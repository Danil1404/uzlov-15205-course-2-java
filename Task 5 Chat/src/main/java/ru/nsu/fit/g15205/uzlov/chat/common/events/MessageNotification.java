package ru.nsu.fit.g15205.uzlov.chat.common.events;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import ru.nsu.fit.g15205.uzlov.chat.common.core.TagNamesConstants;
import ru.nsu.fit.g15205.uzlov.chat.common.messages.TextMessage;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

/**
 * Created by Danil on 2017-07-15.
 */
public class MessageNotification extends Notification {
    private final TextMessage m;
    public MessageNotification(TextMessage m) {
        if (m == null) throw new IllegalArgumentException();
        this.m = m;
    }
    @Override
    public TextMessage getContent() {
        return m;
    }
    @Override
    protected Document generateDocument() {
        try {
            Document d = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
            Element c = d.createElement(TagNamesConstants.ROOT_NOTIFICATION);
            c.setAttribute(TagNamesConstants.COMMAND_NAME_IDENTIFIER, TagNamesConstants.COMMAND_NOTIFICATION_NEW_MESSAGE);
            d.appendChild(c);

            Element m = d.createElement(TagNamesConstants.SUBTAG_MESSAGE_TEXT);
            m.setTextContent(this.m.getContent());
            c.appendChild(m);

            Element n = d.createElement(TagNamesConstants.SUBTAG_NICKNAME);
            n.setTextContent(this.m.getFrom());
            c.appendChild(n);

            Element fid = d.createElement(TagNamesConstants.SUBTAG_FROM_ID);
            fid.setTextContent(this.m.getFromID());
            c.appendChild(fid);

            Element t = d.createElement(TagNamesConstants.SUBTAG_TIME);
            t.setTextContent(formatTime(this.m.getTime()));
            c.appendChild(t);

            return d;
        } catch (ParserConfigurationException e) {
            throw new RuntimeException(e);
        }
    }
}
