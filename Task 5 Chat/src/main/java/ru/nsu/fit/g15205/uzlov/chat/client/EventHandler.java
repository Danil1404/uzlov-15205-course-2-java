package ru.nsu.fit.g15205.uzlov.chat.client;

import ru.nsu.fit.g15205.uzlov.chat.common.core.Event;

/**
 * Created by Danil on 2017-07-17.
 */
public interface EventHandler {
    void handleEvent(Event e);
}
