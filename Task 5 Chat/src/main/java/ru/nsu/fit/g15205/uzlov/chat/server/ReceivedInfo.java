package ru.nsu.fit.g15205.uzlov.chat.server;

import ru.nsu.fit.g15205.uzlov.chat.common.core.Event;

/**
 * Created by Danil on 2017-07-15.
 */
class ReceivedInfo {
    private final Event e;
    private final LinkToUser source;
    ReceivedInfo(Event event, LinkToUser source) {
        this.e = event;
        this.source = source;
    }
    Event getEvent() {
        return e;
    }
    LinkToUser getSource() {
        return source;
    }
}
