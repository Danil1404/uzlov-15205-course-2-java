package ru.nsu.fit.g15205.uzlov.chat.common.core;

import org.w3c.dom.Document;

/**
 * Created by Danil on 2017-07-12.
 */
public interface Event {
    Document asDocument();
    Object getContent();
}
