package ru.nsu.fit.g15205.uzlov.chat.common.network;

import ru.nsu.fit.g15205.uzlov.chat.common.core.BytesDocumentSerializer;
import ru.nsu.fit.g15205.uzlov.chat.common.core.Event;
import ru.nsu.fit.g15205.uzlov.chat.common.core.SendTask;
import ru.nsu.fit.g15205.uzlov.chat.common.core.SerializingException;

public class SimpleSendTask implements SendTask {
    private final Event e;
    public SimpleSendTask(Event e) {
        if (e == null) throw new RuntimeException("Event can't be null");
        this.e = e;
    }
    @Override
    public byte[] asBytes(BytesDocumentSerializer serializer) throws SerializingException {
        return serializer.serialize(e.asDocument());
    }
    @Override
    public Event getEvent() {
        return e;
    }
}
