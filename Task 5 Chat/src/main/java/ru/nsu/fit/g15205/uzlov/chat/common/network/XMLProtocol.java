package ru.nsu.fit.g15205.uzlov.chat.common.network;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import ru.nsu.fit.g15205.uzlov.chat.common.core.*;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

/**
 * Created by Danil on 2017-07-15.
 */
public class XMLProtocol implements Protocol {
    private static final Logger l = LogManager.getLogger();
    private final Socket socket;
    private final DataInputStream input;
    private final DataOutputStream output;
    private final DocumentEventParser parser;
    private final BytesDocumentSerializer serializer;
    XMLProtocol(Socket socket, DocumentEventParser parser, BytesDocumentSerializer serializer) throws IOException, SerializingException {
        this.socket = socket;
        this.parser = parser;
        this.serializer = serializer;
        this.input = new DataInputStream(socket.getInputStream());
        this.output = new DataOutputStream(socket.getOutputStream());
        l.debug("successfully created XMLProtocol object");
    }

    @Override
    public void send(SendTask task) throws NetworkException {
        try {
            byte[] buf = task.asBytes(serializer);
            output.writeInt(buf.length);
            output.write(buf);
            output.flush();
            l.info("Sent message {} to {}", task.getEvent().getClass(), socket);
        } catch (IOException e) {
            l.error("can't write bytes into socket {}, cause: {}", socket, e);
            throw new NetworkException("can't write bytes into socket", e);
        } catch (SerializingException e) {
            l.error("can't serialize Event {}, cause: {}", task.getEvent().asDocument(), e);
            throw new NetworkException("can't serialize Event", e);
        }
    }
    @Override
    public void send(Event event) throws NetworkException {
        this.send(new SimpleSendTask(event));
    }
    @Override
    public Event getEvent() throws ParsingException, NetworkException {
        try {
            int size = input.readInt();
            l.debug("incoming packet from {}, size = " + size, socket);
            if (size <= 0) {
                l.error("packet size from {} is less than 1", socket);
                throw new NetworkException("packet size is less than 1");
            }
            byte[] buf = new byte[size];
            int read = 0;
            while (read < size) {
                read += input.read(buf, read, size - read);
            }
            Document document = serializer.deserialize(buf);
            Event e = parser.parse(document);
            l.info("successfully read Event {} from {}", e.getClass(), socket);
            l.debug("read Event {} (as document: {} ) from {}", e.getClass(), e.asDocument(),socket);
            return e;
        } catch (IOException e) {
            l.error("can't read bytes from socket {}, cause:", socket, e);
            throw new NetworkException("can't read bytes from socket", e);
        } catch (SerializingException e) {
            throw new ParsingException("can't parse document", e);
        }
    }
    @Override
    public void close() {
        l.info("closing socket {}", socket);
        try {
            socket.close();
        } catch (IOException e) {
            l.error("can't close socket {}, cause: {}", socket, e);
            e.printStackTrace();
        }
    }
}
