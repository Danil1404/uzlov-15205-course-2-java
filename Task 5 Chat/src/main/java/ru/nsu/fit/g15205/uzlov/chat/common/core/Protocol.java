package ru.nsu.fit.g15205.uzlov.chat.common.core;

import java.io.Closeable;

/**
 * Created by Danil on 2017-07-13.
 */
public interface Protocol extends Closeable {
    void send(SendTask task) throws NetworkException;
    void send(Event event) throws NetworkException;
    Event getEvent() throws NetworkException, ParsingException;
    void close();
}
