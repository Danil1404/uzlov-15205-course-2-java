package ru.nsu.fit.g15205.uzlov.chat.common.network;

import ru.nsu.fit.g15205.uzlov.chat.common.core.Protocol;
import ru.nsu.fit.g15205.uzlov.chat.common.core.SerializingException;
import ru.nsu.fit.g15205.uzlov.chat.common.events.StandardDocumentEventParser;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.SocketException;

/**
 * Created by Danil on 2017-07-13.
 */
public class ProtocolFactory {
    private static final int CLIENT_TIMEOUT = 60 * 60 * 1000;
    private static void setSocketTimeout(Socket socket) {
        try {
            socket.setSoTimeout(CLIENT_TIMEOUT);
        } catch (SocketException e) {
            throw new RuntimeException(e);
        }
    }
    public static Protocol getXMLProtocol(Socket socket) {
        setSocketTimeout(socket);
        try {
            return new XMLProtocol(socket, new StandardDocumentEventParser(), new XMLBytesDocumentSerializer());
        } catch (IOException | SerializingException e) {
            throw new RuntimeException(e);
        }
    }
    public static Protocol getObjectStreamProtocol(Socket socket) {
        setSocketTimeout(socket);
        try {
            return new ObjectStreamProtocol(socket, new StandardDocumentEventParser());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
