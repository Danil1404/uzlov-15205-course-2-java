package ru.nsu.fit.g15205.uzlov.chat.client.GUI;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.nsu.fit.g15205.uzlov.chat.client.Dialogue;
import ru.nsu.fit.g15205.uzlov.chat.client.LinkToServer;
import ru.nsu.fit.g15205.uzlov.chat.common.events.*;
import ru.nsu.fit.g15205.uzlov.chat.common.messages.PresenceInfo;
import ru.nsu.fit.g15205.uzlov.chat.common.messages.TextMessage;
import ru.nsu.fit.g15205.uzlov.chat.common.core.UserInfo;
import ru.nsu.fit.g15205.uzlov.chat.common.core.VisibleMessage;

import javax.swing.*;
import java.awt.*;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by Danil on 2017-07-10.
 */
public class ClientGUI {
    private static final Logger l = LogManager.getLogger();
    private final JPanel panel = new JPanel(new GridBagLayout());
    private final JPanel dialoguesPanel = new JPanel(new BorderLayout()); // Если не использоать new BorderLayout(), то невозможно прокручивать список
    private final JPanel conversationPanel = new JPanel(new BorderLayout());
    private final Map<Dialogue, JPanel> chats = new TreeMap<>();
    private final DefaultListModel<Dialogue> listModel = new DefaultListModel<>();
    private final JList<Dialogue> dialoguesList = new JList<>(listModel);
    private Dialogue selectedDialogue = null;
    public ClientGUI() {
        generateGUI();
    }
    private void generateGUI() {
        l.info("generating GUI");

        // Панель слева, в ней должен быть список чатов
        Dimension d = new Dimension(200, 100);
        dialoguesPanel.setSize(d);
        dialoguesPanel.setMinimumSize(d);
        dialoguesPanel.setPreferredSize(d);
        dialoguesPanel.setMaximumSize(d);
        dialoguesPanel.setBackground(Color.CYAN);
        dialoguesList.setCellRenderer(new DialogueListCellRenderer(d));
        dialoguesList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        dialoguesList.addListSelectionListener(e -> {
            if (!e.getValueIsAdjusting()) {
                SwingUtilities.invokeLater(() -> showDialogue(listModel.elementAt(dialoguesList.getSelectedIndex())));
            }
        });
        dialoguesPanel.add(new JScrollPane(dialoguesList, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER));

        // Панель справа, в ней отображаются сообщения конкретного чата
        conversationPanel.setBackground(Color.YELLOW);
        conversationPanel.add(new JLabel("Choose any dialogue or create one"));

        GridBagConstraints c = new GridBagConstraints();
        c.gridwidth = 1;
        c.fill = GridBagConstraints.BOTH;
        c.gridx = 0;
        c.gridy = 1;
        c.weighty = 1;
        panel.add(dialoguesPanel, c);
        c.gridx = 1;
        c.weightx = 1;
        panel.add(conversationPanel, c);
    }
    public void showGUI(JFrame frame) {
        l.info("showing GUI");

        JMenuBar menuBar = new JMenuBar();
        JMenu menu = new JMenu("Меню");
        JMenuItem menuItemNewConnection = new JMenuItem("Новое соединение");
        menuItemNewConnection.setAccelerator(KeyStroke.getKeyStroke("F2"));
        menuItemNewConnection.addActionListener(e -> SwingUtilities.invokeLater(() -> {
            JPanel serverPicking = new JPanel();
            serverPicking.setLayout(new BoxLayout(serverPicking, BoxLayout.Y_AXIS));

            JPanel tmp = new JPanel();
            tmp.add(new JLabel("Название (любое)"));
            JTextField connectionNameField = new JTextField("без названия", 15);
            tmp.add(connectionNameField);
            serverPicking.add(tmp);

            tmp = new JPanel();
            tmp.add(new JLabel("Никнейм"));
            JTextField nicknameField = new JTextField("ghost",  15);
            tmp.add(nicknameField);
            serverPicking.add(tmp);

            tmp = new JPanel();
            tmp.add(new JLabel("Адрес сервера"));
            //JTextField addressField = new JTextField("example.com", 15);
            JTextField addressField = new JTextField("127.0.0.1", 15);
            tmp.add(addressField);
            serverPicking.add(tmp);

            tmp = new JPanel();
            tmp.add(new JLabel("Порт"));
            JTextField portField = new JTextField("65003", 5);
            tmp.add(portField);
            serverPicking.add(tmp);

            int result = JOptionPane.showConfirmDialog(frame, serverPicking, "Новое соединение", JOptionPane.OK_CANCEL_OPTION);
            if (result != JOptionPane.OK_OPTION) {
                return;
            }
            int port;
            try {
                port = Integer.parseInt(portField.getText());
            } catch (NumberFormatException ex) {
                l.error("can't parse {} as port number: {}", portField.getText(), ex);
                JOptionPane.showMessageDialog(frame, "\"" + portField.getText() + "\" is not a number.", "Error", JOptionPane.ERROR_MESSAGE);
                return;
            }
            if (port < 1 || port > 65535) {
                l.error("port number should be between 1 and 65535: " + port);
                JOptionPane.showMessageDialog(frame, "port should be between 1 and 65535", "Error", JOptionPane.ERROR_MESSAGE);
                return;
            }
            InetAddress inetAddress;
            try {
                inetAddress = InetAddress.getByName(addressField.getText());
            } catch (UnknownHostException ex) {
                l.error("can't parse InetAddress from hostname {}: {}", addressField.getText(), ex);
                JOptionPane.showMessageDialog(frame, "Can't parse \"" + addressField.getText() + "\" as address", "Error", JOptionPane.ERROR_MESSAGE);
                return;
            }
            Socket socket;
            try {
                socket = new Socket(inetAddress.getHostAddress(), port);
            } catch (IOException ex) {
                l.error("can't create socket: {}", ex);
                JOptionPane.showMessageDialog(frame, "Can't connect to \"" + inetAddress.getHostAddress() + "\": " + ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                return;
            }
            Dialogue dialogue = new Dialogue(connectionNameField.getText(), nicknameField.getText(), addressField.getText());
            LinkToServer link = new LinkToServer(socket, event -> SwingUtilities.invokeLater(() -> {
                if (event instanceof Notification) {
                    l.debug("incoming notification");
                    if (event instanceof MessageNotification) {
                        TextMessage m = (TextMessage)event.getContent();
                        dialogue.addMessage(m);
                    }
                    if (event instanceof LoginNotification || event instanceof LogoutNotification) {
                        PresenceInfo m = (PresenceInfo)event.getContent();
                        dialogue.addMessage(m);
                    }
                    l.error("unknown notification");
                    return;
                }
                if (event instanceof SuccessAnswer) {
                    l.debug("incoming success answer");
                    if (event instanceof UserListAnswer) {
                        dialogue.setUserList((List<UserInfo>)event.getContent());
                        return;
                    }
                    return;
                }
                if (event instanceof ErrorAnswer) {
                    l.debug("incoming error answer");
                    Object t = event.getContent();
                    String s;
                    if (t != null) {
                        s = (String)t;
                    } else {
                        s = "Unknown error";
                    }
                    JOptionPane.showMessageDialog(frame, new JLabel(s), "Error", JOptionPane.ERROR_MESSAGE);
                    return;
                }
                //JOptionPane.showMessageDialog(frame, new JLabel("ConnectionsListener is incompatible with your client"), "Error", JOptionPane.ERROR_MESSAGE);
                l.error("unknown incoming event: {}", event.getClass());
            }), nicknameField.getText());
            createDialoguePanel(dialogue, link);
            link.start();
        }));


        JMenuItem menuItemExit = new JMenuItem("Выйти");
        menuItemExit.addActionListener(e -> System.exit(0));

        menu.add(menuItemNewConnection);
        menu.add(menuItemExit);
        menuBar.add(menu);

        frame.setJMenuBar(menuBar);

        Dimension d = frame.getSize();
        frame.getContentPane().removeAll();
        frame.setSize(d);
        frame.add(panel);
        frame.pack();
        frame.getContentPane().repaint();
    }
    private void showDialogue(Dialogue dialogue) {
        l.info("showing dialogue with name {}", dialogue.getName());
        JPanel con = chats.get(dialogue);
        if (con == null) {
            l.error("dialogue {} is not registered and doesn't have a JPanel", dialogue.getName());
            throw new IllegalArgumentException("Can't find such dialogue");
        }
        if (selectedDialogue != null)
            selectedDialogue.unselect();
        selectedDialogue = dialogue;
        selectedDialogue.setSelected();
        conversationPanel.removeAll();
        conversationPanel.add(con, BorderLayout.CENTER);
        conversationPanel.revalidate();
        conversationPanel.repaint();
    }
    private void createDialoguePanel(Dialogue dialogue, LinkToServer link) {
        l.info("creating JPanel for dialogue {}", dialogue.getName());
        if (chats.containsKey(dialogue)) {
            l.error("dialogue {} is already has it's JPanel", dialogue.getName());
            throw new IllegalArgumentException("Can't add one dialogue two times");
        }
        JPanel con = new JPanel(new BorderLayout());
        JPanel dialogueHeaderPanel = new JPanel(new BorderLayout());
        JPanel messagesPanel = new JPanel(new BorderLayout());
        JPanel typingPanel = new JPanel(new BorderLayout());

        JLabel nickname = new JLabel("Nick: " + dialogue.getNickname() + " >соединение< ");
        dialogueHeaderPanel.add(nickname, BorderLayout.WEST);
        link.setUidUpdater(e -> {
            nickname.setText("Nick: " + dialogue.getNickname() + " #" + link.getInfo().getUID());
        });
        JButton showUserLIstButton = new JButton("users");
        JPanel popup = new JPanel(new BorderLayout());
        JPanel panelWithUserList = new JPanel();
        JScrollPane userListScrollPane = new JScrollPane(panelWithUserList, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        popup.add(userListScrollPane, BorderLayout.CENTER);
        JButton ret = new JButton("назад");

        popup.add(ret, BorderLayout.SOUTH);
        panelWithUserList.setLayout(new BoxLayout(panelWithUserList, BoxLayout.Y_AXIS));
        dialogue.setNewUserListListener(userList -> {
            panelWithUserList.removeAll();
            for (UserInfo i : userList) {
                JPanel userPanel = new JPanel();
                userPanel.add(new JLabel(i.getName()));
                userPanel.add(new JLabel("#" + i.getUID()));
                userPanel.add(new JLabel(i.getClient()));
                panelWithUserList.add(userPanel);
            }
            messagesPanel.revalidate();
            messagesPanel.repaint();
        });
        JPanel tmp = new JPanel();
        tmp.add(showUserLIstButton);
        dialogueHeaderPanel.add(tmp);

        dialogueHeaderPanel.add(new JLabel("Server: " + dialogue.getServerName()), BorderLayout.EAST);

        DefaultListModel<VisibleMessage> messages = new DefaultListModel<>();
        JList<VisibleMessage> messagesList = new JList<>(messages);
        messagesList.setCellRenderer(new MessagesListCellRenderer(conversationPanel));
        for (VisibleMessage m : new ArrayList<>(dialogue.getMessagesList())) {
            // Я не уверен, что если во время итерации будут добавлены новые сообщения, то все будет нормально,
            // поэтому на всякий случай создаю копию списка сообщений
            messages.addElement(m);
        }
        listModel.addElement(dialogue);
        dialogue.setNewMessageListener(message -> SwingUtilities.invokeLater(() -> {
            l.debug("adding new message to messages list in GUI");
            messages.addElement(message);
            if (messagesList.getLastVisibleIndex() >= messagesList.getModel().getSize() - 2) {
                messagesList.ensureIndexIsVisible(messagesList.getModel().getSize() - 1);
            }
            dialoguesPanel.revalidate();
            dialoguesPanel.repaint();
        }));

        JScrollPane messagesScrollPane = new JScrollPane(messagesList, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        messagesPanel.add(messagesScrollPane);

        JTextArea textArea = new JTextArea();
        textArea.setLineWrap(true);
        textArea.setWrapStyleWord(true);
        textArea.setRows(2);

        JButton sendButton = new JButton("send");
        sendButton.addActionListener(e -> {
            l.debug("trying to send new message in dialogue {}", dialogue.getName());
            if (!link.isActive()) {
                l.error("sending message to dead dialogue");
                sendButton.setText("error");
                return;
            }
            Message m = new Message(textArea.getText(), "");
            link.send(m);
            UserInfo info = link.getInfo();
            dialogue.addMessage(new TextMessage(info.getName(), info.getUID(), textArea.getText(), Instant.now()));
            l.info("sent message {} to dialogue {}", textArea.getText(), dialogue.getName());
            textArea.setText("");
        });

        typingPanel.add(textArea, BorderLayout.CENTER);
        typingPanel.add(sendButton, BorderLayout.EAST);

        con.add(dialogueHeaderPanel, BorderLayout.NORTH);
        con.add(messagesPanel, BorderLayout.CENTER);
        con.add(typingPanel, BorderLayout.SOUTH);

        chats.put(dialogue, con);

        con.addComponentListener(new ComponentAdapter() {
            public void componentResized(ComponentEvent evt) {
                messagesList.setFixedCellHeight(10);
                messagesList.setFixedCellHeight(-1);
            }
        });


        showUserLIstButton.addActionListener(e -> SwingUtilities.invokeLater(() -> {
            panelWithUserList.removeAll();
            panelWithUserList.add(new JLabel("Получение информации с сервера, подождите"));
            link.send(new UserListRequest(link.getInfo().getUID()));
            messagesPanel.removeAll();
            messagesPanel.add(popup);
            messagesPanel.revalidate();
            messagesPanel.repaint();
        }));

        ret.addActionListener(e -> {
            messagesPanel.removeAll();
            messagesPanel.add(messagesScrollPane);
            messagesPanel.revalidate();
            messagesPanel.repaint();
        });
    }
}
