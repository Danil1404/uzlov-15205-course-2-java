package ru.nsu.fit.g15205.uzlov.chat.common.events;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import ru.nsu.fit.g15205.uzlov.chat.common.core.TagNamesConstants;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

/**
 * Created by Danil on 2017-07-15.
 */
public class UserListRequest extends EventTemplate {
    private final String id;
    public UserListRequest(String id) {
        assert id != null;
        this.id = id;
    }
    @Override
    public String getContent() {
        return null;
    }

    @Override
    protected Document generateDocument() {
        try {
            Document d = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
            Element s = d.createElement(TagNamesConstants.ROOT_REQUEST);
            s.setAttribute(TagNamesConstants.COMMAND_NAME_IDENTIFIER, TagNamesConstants.COMMAND_REQUEST_USER_LIST_REQUEST);
            d.appendChild(s);

            Element usid = d.createElement(TagNamesConstants.SUBTAG_UID);
            usid.setTextContent(id);
            s.appendChild(usid);

            return d;
        } catch (ParserConfigurationException e) {
            throw new RuntimeException(e);
        }
    }
}
