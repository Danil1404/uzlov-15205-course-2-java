package ru.nsu.fit.g15205.uzlov.chat.client;

import ru.nsu.fit.g15205.uzlov.chat.common.core.UserInfo;
import ru.nsu.fit.g15205.uzlov.chat.common.core.VisibleMessage;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by Danil on 2017-07-11.
 */
public class Dialogue implements Comparable<Dialogue> {
    private static final AtomicInteger id = new AtomicInteger(0);
    private final int dialID;
    private final List<VisibleMessage> messagesList = Collections.synchronizedList(new ArrayList<>());
    private String name;
    private final String nickname;
    private final String serverName;
    private int unreadCount = 0;
    private boolean isShown = false;
    private NewMessageListener newMessageListener = null;
    private List<UserInfo> userList = null;
    private NewUserListListener newUserListListener = null;

    public interface NewMessageListener {

        void newMessage(VisibleMessage message);
    }
    public interface NewUserListListener {
        void update(List<UserInfo> userList);
    }
    @Override
    public int compareTo(Dialogue o) {
        return Integer.compare(dialID, o.dialID);
    }

    public List<UserInfo> getUserList() {
        return userList;
    }
    public void setUserList(List<UserInfo> userList) {
        this.userList = userList;
        if (newUserListListener != null) {
            newUserListListener.update(userList);
        }
    }
    public void setNewUserListListener(NewUserListListener newUserListListener) {
        this.newUserListListener = newUserListListener;
    }

    public Dialogue (String name, String nickname, String serverName) {
        if (name == null || nickname == null || serverName == null) throw new IllegalArgumentException("null argument is prohibited");
        this.name = name;
        this.nickname = nickname;
        this.serverName = serverName;
        dialID = id.getAndIncrement();
    }
    public void setName(String name) {
        if (name == null) throw new IllegalArgumentException("name can't be null");
        this.name = name;
    }
    public void setNewMessageListener(NewMessageListener newMessageListener) {
        this.newMessageListener = newMessageListener;
    }
    public String getName() {
        return name;
    }
    public int getUnreadCount() {
        return unreadCount;
    }
    public void addMessage(VisibleMessage message) {
        if (message == null) throw new IllegalArgumentException("TextMessage can't be null");
        messagesList.add(message);
        if (newMessageListener != null)
            newMessageListener.newMessage(message);
        if (!isShown)
            unreadCount++;
    }
    public List<VisibleMessage> getMessagesList() {
        return messagesList;
    }
    public void setSelected() {
        unreadCount = 0;
        isShown = true;
    }
    public void unselect() {
        isShown = false;
    }
    public String getLastMessageContent() {
        if (!messagesList.isEmpty())
            return messagesList.get(messagesList.size() - 1).getContent();
        else
            return "--  no messages  --";
    }
    public String getNickname() {
        return nickname;
    }
    public String getServerName() {
        return serverName;
    }
}
