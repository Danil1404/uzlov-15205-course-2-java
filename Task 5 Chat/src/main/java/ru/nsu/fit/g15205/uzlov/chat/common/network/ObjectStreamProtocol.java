package ru.nsu.fit.g15205.uzlov.chat.common.network;

import org.w3c.dom.Document;
import ru.nsu.fit.g15205.uzlov.chat.common.core.*;
import ru.nsu.fit.g15205.uzlov.chat.common.events.StandardDocumentEventParser;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

/**
 * Created by Danil on 2017-07-15.
 */
public class ObjectStreamProtocol implements Protocol {
    private final Socket socket;
    private final ObjectInputStream input;
    private final ObjectOutputStream output;
    private final DocumentEventParser parser;
    public ObjectStreamProtocol(Socket socket, DocumentEventParser parser) throws IOException {
        this.socket = socket;
        this.parser = parser;
        this.input = new ObjectInputStream(socket.getInputStream());
        this.output = new ObjectOutputStream(socket.getOutputStream());
    }
    @Override
    public void send(SendTask task) throws NetworkException {
        send(task.getEvent());
    }
    @Override
    public void send(Event event) throws NetworkException {
        try {
            output.writeObject(event.asDocument());
            output.flush();
        } catch (IOException e) {
            throw new NetworkException("can't write object into socket", e);
        }
    }
    @Override
    public Event getEvent() throws NetworkException, ParsingException {
        try {
            return parser.parse((Document)input.readObject());
        } catch (IOException e) {
            throw new NetworkException("can't read object from socket", e);
        } catch (ClassNotFoundException e) {
            throw new NetworkException("read object is not a Document", e);
        }
    }
    @Override
    public void close() {
        try {
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
