package ru.nsu.fit.g15205.uzlov.chat.common.core;

/**
 * Created by Danil on 2017-07-16.
 */
public class ParsingException extends Exception {
    public ParsingException() {}
    public ParsingException(String reason) {
        super(reason);
    }
    public ParsingException(Throwable t) {
        super(t);
    }
    public ParsingException(String s, Throwable t) {
        super(s, t);
    }
}
