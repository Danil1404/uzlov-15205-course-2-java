package ru.nsu.fit.g15205.uzlov.chat.common.core;

import org.w3c.dom.Document;

/**
 * Created by Danil on 2017-07-15.
 */
public interface BytesDocumentSerializer {
    byte[] serialize(Document d) throws SerializingException;
    Document deserialize(byte[] b) throws SerializingException;
}
