package ru.nsu.fit.g15205.uzlov.chat.server;

import ru.nsu.fit.g15205.uzlov.chat.server.ReceivedInfo;

/**
 * Created by Danil on 2017-07-15.
 */
interface EventHandler {
    void handleEvent(ReceivedInfo info);
}
