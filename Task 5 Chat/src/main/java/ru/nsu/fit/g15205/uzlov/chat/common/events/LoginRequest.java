package ru.nsu.fit.g15205.uzlov.chat.common.events;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import ru.nsu.fit.g15205.uzlov.chat.common.core.TagNamesConstants;
import ru.nsu.fit.g15205.uzlov.chat.common.core.UserInfo;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

/**
 * Created by Danil on 2017-07-15.
 */
public class LoginRequest extends EventTemplate {
    private final UserInfo info;
    public LoginRequest(UserInfo info) {
        if (info == null)throw new IllegalArgumentException();
        this.info = info;
    }
    @Override
    public UserInfo getContent() {
        return info;
    }

    @Override
    protected Document generateDocument() {
        try {
            Document d = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
            Element c = d.createElement(TagNamesConstants.ROOT_REQUEST);
            c.setAttribute(TagNamesConstants.COMMAND_NAME_IDENTIFIER, TagNamesConstants.COMMAND_REQUEST_LOGIN);
            d.appendChild(c);

            Element n = d.createElement(TagNamesConstants.SUBTAG_NICKNAME);
            n.setTextContent(info.getName());
            c.appendChild(n);

            Element t = d.createElement(TagNamesConstants.SUBTAG_CLIENT_NAME);
            t.setTextContent(info.getClient());
            c.appendChild(t);

            return d;
        } catch (ParserConfigurationException e) {
            throw new RuntimeException(e);
        }
    }
}
