package ru.nsu.fit.g15205.uzlov.chat.common.messages;

import ru.nsu.fit.g15205.uzlov.chat.common.core.VisibleMessage;

import java.time.Instant;

/**
 * Created by Danil on 2017-07-10.
 */
public class TextMessage implements VisibleMessage {
    private final String content;
    private final String from;
    private final String fromID;
    private final Instant time;
    public TextMessage(String from, String fromID, String content, Instant time) {
        assert from != null;
        this.from = from;

        assert fromID != null;
        this.fromID = fromID;

        assert content != null;
        this.content = content;

        assert time != null;
        this.time = time;
    }
    @Override
    public String getContent() {
        return content;
    }
    @Override
    public String getFrom() {
        return from;
    }
    @Override
    public String getFromID() {
        return fromID;
    }
    @Override
    public Instant getTime() {
        return time;
    }
}
