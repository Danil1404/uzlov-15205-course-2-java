package ru.nsu.fit.g15205.uzlov.chat.common.core;

/**
 * Created by Danil on 2017-07-15.
 */
public interface SendTask {
    byte[] asBytes(BytesDocumentSerializer serializer) throws SerializingException;
    Event getEvent();
}
