package ru.nsu.fit.g15205.uzlov.chat.common.events;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import ru.nsu.fit.g15205.uzlov.chat.common.core.TagNamesConstants;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

/**
 * Created by Danil on 2017-07-15.
 */
public class ErrorAnswer extends Answer {
    private final String reason;
    public ErrorAnswer(String reason) {
        this.reason = reason;
    }
    @Override
    public String getContent() {
        return reason;
    }
    @Override
    protected Document generateDocument() {
        try {
            Document d = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
            Element s = d.createElement(TagNamesConstants.ROOT_ERROR);
            d.appendChild(s);

            if (reason != null) {
                Element r = d.createElement(TagNamesConstants.SUBTAG_ERROR_TEXT);
                r.setTextContent(reason);
                s.appendChild(r);
            }

            return d;
        } catch (ParserConfigurationException e) {
            throw new RuntimeException(e);
        }
    }
}
