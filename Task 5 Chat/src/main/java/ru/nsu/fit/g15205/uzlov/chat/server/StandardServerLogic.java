package ru.nsu.fit.g15205.uzlov.chat.server;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.nsu.fit.g15205.uzlov.chat.common.core.Event;
import ru.nsu.fit.g15205.uzlov.chat.common.messages.TextMessage;
import ru.nsu.fit.g15205.uzlov.chat.common.core.SendTask;
import ru.nsu.fit.g15205.uzlov.chat.common.core.UserInfo;
import ru.nsu.fit.g15205.uzlov.chat.common.events.*;
import ru.nsu.fit.g15205.uzlov.chat.common.network.CachedSendTask;

import java.time.Instant;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Created by Danil on 2017-07-15.
 */
class StandardServerLogic implements Runnable, ServerLogic {
    private static final Logger l = LogManager.getLogger();
    private final BlockingQueue<ReceivedInfo> queue = new LinkedBlockingQueue<>();
    private final List<LinkToUser> activeUsers = new LinkedList<>();
    private final List<UserInfo> userInfoCache = new ArrayList<>();
    private final List<LinkToUser> unregisteredUsers = new LinkedList<>();
    private long maxUID = 1;
    private final List<SendTask> chatHistory = new ArrayList<>();
    public void handleEvent(ReceivedInfo info) {
        queue.offer(info);
    }
    public void addUser(LinkToUser linkToUser) {
        unregisteredUsers.add(linkToUser);
        linkToUser.start();
    }
    @Override
    public void run() {
        l.debug("running server logic thread");
        try {
            while (true) {
                ReceivedInfo info = queue.take();
                Event e = info.getEvent();
                l.trace("incoming event: {}", e.getClass());
                LinkToUser linkToUser = info.getSource();

                if (e instanceof LoginRequest) {
                    l.trace("new login request");
                    if (linkToUser.getUserInfo() == null) {
                        l.error("login request: linkToUser info is null");
                        continue;
                    }
                    // Проверили все условия, которые мешают зарегистрироваться
                    if (!unregisteredUsers.contains(linkToUser)) {
                        l.error("login request from registered linkToUser {}", linkToUser);
                        linkToUser.send(new ErrorAnswer("second login"));
                        linkToUser.stop();
                        continue;
                    }
                    linkToUser.setId("" + maxUID);
                    maxUID++;
                    SendTask t = new CachedSendTask(new LoginNotification(linkToUser.getUserInfo().getName(), linkToUser.getId(), Instant.now()));
                    chatHistory.add(t);
                    for(LinkToUser u : activeUsers) {
                        u.send(t);
                    }
                    unregisteredUsers.remove(linkToUser);
                    activeUsers.add(linkToUser);
                    userInfoCache.add(linkToUser.getUserInfo());
                    linkToUser.send(new SuccessAnswer(linkToUser.getId()));
                    for (SendTask s : chatHistory) {
                        linkToUser.send(s);
                    }
                    continue;
                }
                if (e instanceof UserListRequest) {
                    l.trace("UserListRequest from {}", linkToUser);
                    linkToUser.send(new UserListAnswer(userInfoCache));
                    continue;
                }
                if (e instanceof Message) {
                    l.trace("message from linkToUser {}", linkToUser);
                    linkToUser.send(new SuccessAnswer());
                    TextMessage message = new TextMessage(linkToUser.getUserInfo().getName(), linkToUser.getId(), (String) e.getContent(), Instant.now());
                    SendTask t = new CachedSendTask(new MessageNotification(message));
                    chatHistory.add(t);
                    for(LinkToUser u : activeUsers) {
                        if (u != linkToUser)
                            u.send(t);
                    }
                    continue;
                }
                if (e instanceof LogoutRequest) {
                    l.trace("LogoutRequest from linkToUser {}", linkToUser);
                    activeUsers.remove(linkToUser);
                    userInfoCache.remove(linkToUser.getUserInfo());
                    linkToUser.send(new SuccessAnswer());
                    SendTask t = new CachedSendTask(new LogoutNotification(linkToUser.getUserInfo().getName(), linkToUser.getId(), Instant.now()));
                    chatHistory.add(t);
                    for(LinkToUser u : activeUsers) {
                        u.send(t);
                    }
                    continue;
                }
                if (e instanceof ConnectionError) {
                    l.trace("ConnectionError from linkToUser {}", linkToUser);
                    activeUsers.remove(linkToUser);
                    userInfoCache.remove(linkToUser.getUserInfo());
                    SendTask t = new CachedSendTask(new LogoutNotification(linkToUser.getUserInfo().getName(), linkToUser.getId(), Instant.now()));
                    chatHistory.add(t);
                    for(LinkToUser u : activeUsers) {
                        u.send(t);
                    }
                    continue;
                }
                l.error("unknown command from linkToUser {}", linkToUser);
                linkToUser.send(new ErrorAnswer("unknown command"));
            }
        } catch (InterruptedException e) {
            l.info("interrupted server logic thread, stopping user handlers");
            for(LinkToUser linkToUser : unregisteredUsers) {
                linkToUser.stop();
            }
            for(LinkToUser linkToUser : activeUsers) {
                linkToUser.stop();
            }
        }
    }
}
