package ru.nsu.fit.g15205.uzlov.chat.common.events;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import ru.nsu.fit.g15205.uzlov.chat.common.core.*;
import ru.nsu.fit.g15205.uzlov.chat.common.messages.TextMessage;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.SplittableRandom;

/**
 * Created by Danil on 2017-07-15.
 */
public class StandardDocumentEventParser implements DocumentEventParser {
    private static final Logger l = LogManager.getLogger();
    @Override
    public Event parse(Document d) throws ParsingException {
        l.trace("parsing {}", d);
        Element root = d.getDocumentElement();
        switch (root.getTagName()) {
            case TagNamesConstants.ROOT_SUCCESS: {
                return parseSuccess(root);
            }
            case TagNamesConstants.ROOT_REQUEST: {
                return parseCommand(root);
            }
            case TagNamesConstants.ROOT_NOTIFICATION: {
                return parseEvent(root);
            }
            case TagNamesConstants.ROOT_ERROR: {
                return parseError(root);
            }
            default: {
                l.error("unknown root document tag: {}", root.getTagName());
                throw new ParsingException("unknown root tag name: " + root.getTagName());
            }
        }
    }
    private Event parseSuccess(Element root) {
        NodeList nl = root.getElementsByTagName(TagNamesConstants.SUBTAG_USER_LIST);

        if (nl.getLength() == 0) {
            l.debug("parsing empty success event");
            String s = getFirstValueByTagName(root, TagNamesConstants.SUBTAG_UID, null);
            return new SuccessAnswer(s);
        }

        l.debug("parsing success with user list");

        Element users = (Element)nl.item(0);
        NodeList list = users.getElementsByTagName(TagNamesConstants.SUBTAG_USER);
        List<UserInfo> lui = new ArrayList<>();

        for (int i = 0; i < list.getLength(); i++) {
            Element u = (Element)list.item(i);

            NodeList nameList = u.getElementsByTagName(TagNamesConstants.SUBTAG_NICKNAME);
            if (nameList.getLength() < 1) {
                l.warn("success answer: nameList.getLength() < 1");
                continue;
            }
            String name = nameList.item(0).getTextContent();

            NodeList typeList = u.getElementsByTagName(TagNamesConstants.SUBTAG_CLIENT_NAME);
            if (typeList.getLength() < 1) {
                l.warn("success answer: typeList.getLength() < 1");
                continue;
            }
            String client = typeList.item(0).getTextContent();

            UserInfo info = new UserInfo(name, client, "default");

            NodeList uid = u.getElementsByTagName(TagNamesConstants.SUBTAG_UID);
            if (uid.getLength() >= 1) {
                info.setUID(uid.item(0).getTextContent());
            } else {
                info.setUID("-1");
            }

            lui.add(info);
        }

        return new UserListAnswer(lui);
    }
    private Event parseCommand(Element root) throws ParsingException {
        String commandName = root.getAttribute(TagNamesConstants.COMMAND_NAME_IDENTIFIER);
        switch (commandName) {
            case TagNamesConstants.COMMAND_REQUEST_USER_LIST_REQUEST: {
                l.debug("parsing user list request");

                String uid = getSingleValueByTagName(root, TagNamesConstants.SUBTAG_UID);

                return new UserListRequest(uid);
            }
            case TagNamesConstants.COMMAND_REQUEST_SEND_MESSAGE: {
                l.debug("parsing new message event");

                String uid = getSingleValueByTagName(root, TagNamesConstants.SUBTAG_UID);
                String text = getSingleValueByTagName(root, TagNamesConstants.SUBTAG_MESSAGE_TEXT);

                return new Message(text, uid);
            }
            case TagNamesConstants.COMMAND_REQUEST_LOGIN: {
                l.debug("parsing login request");

                String name = getSingleValueByTagName(root, TagNamesConstants.SUBTAG_NICKNAME);
                String type = getSingleValueByTagName(root, TagNamesConstants.SUBTAG_CLIENT_NAME);

                UserInfo info = new UserInfo(name, type, "default");
                return new LoginRequest(info);
            }
            case TagNamesConstants.COMMAND_REQUEST_LOGOUT: {
                l.debug("parsing logout request");

                String uid = getFirstValueByTagName(root, TagNamesConstants.SUBTAG_UID, "no uid");

                return new LogoutRequest(uid);
            }
            default: {
                l.error("unknown command name: {}", commandName);
                throw new ParsingException("unknown command name: " + commandName);
            }
        }
    }
    private Event parseEvent(Element root) throws ParsingException {
        String commandName = root.getAttribute(TagNamesConstants.COMMAND_NAME_IDENTIFIER);
        switch (commandName) {
            case TagNamesConstants.COMMAND_NOTIFICATION_NEW_MESSAGE: {
                l.debug("parsing message notification");

                String text = getSingleValueByTagName(root, TagNamesConstants.SUBTAG_MESSAGE_TEXT);
                String from = getSingleValueByTagName(root, TagNamesConstants.SUBTAG_NICKNAME);
                String fromID = getFirstValueByTagName(root, TagNamesConstants.SUBTAG_FROM_ID, "ndef");
                Instant time = parseTime(root);

                TextMessage m = new TextMessage(from, fromID, text, time);
                return new MessageNotification(m);
            }
            case TagNamesConstants.COMMAND_NOTIFICATION_LOGIN: {
                l.debug("parsing login notification");

                String from = getSingleValueByTagName(root, TagNamesConstants.SUBTAG_NICKNAME);
                String fromID = getFirstValueByTagName(root, TagNamesConstants.SUBTAG_FROM_ID, "ndef");
                Instant time = parseTime(root);

                return new LoginNotification(from, fromID, time);
            }
            case TagNamesConstants.COMMAND_NOTIFICATION_LOGOUT: {
                l.debug("parsing logout notification");

                String from = getSingleValueByTagName(root, TagNamesConstants.SUBTAG_NICKNAME);
                String fromID = getFirstValueByTagName(root, TagNamesConstants.SUBTAG_FROM_ID, "ndef");
                Instant time = parseTime(root);

                return new LogoutNotification(from, fromID, time);
            }
            default: {
                l.error("unknown event name: {}", commandName);
                throw new ParsingException("unknown event name: " + commandName);
            }
        }
    }
    private Event parseError(Element root) {
        l.debug("parsing error");

        NodeList nl = root.getElementsByTagName(TagNamesConstants.SUBTAG_ERROR_TEXT);
        if (nl.getLength() == 0) {
            l.trace("empty error");
            return new ErrorAnswer(null);
        }
        String cause = nl.item(0).getTextContent();

        l.trace("error with cause {}", cause);
        return new ErrorAnswer(cause);
    }
    private Instant parseTime(Element element) {
        NodeList nodeList = element.getElementsByTagName(TagNamesConstants.SUBTAG_TIME);
        Instant time;
        if (nodeList.getLength() != 1) {
            l.debug("event doesn't contain creation time, using current time");
            time = Instant.now();
        } else {
            try {
                time = Instant.from(DateTimeFormatter.ISO_INSTANT.parse(nodeList.item(0).getTextContent()));
            } catch (DateTimeParseException e1) {
                l.debug("can't parse time, using current time: {}", e1);
                time = Instant.now();
            }
        }
        return time;
    }
    private String getSingleValueByTagName(Element element, String tagName) throws ParsingException {
        NodeList nodeList = element.getElementsByTagName(tagName);

        if (nodeList.getLength() < 1) {
            l.error("can't find value of {}", tagName);
            throw new ParsingException("can't find value of " + tagName);
        }
        if (nodeList.getLength() > 1) {
            l.error("multiple values of {}", tagName);
            throw new ParsingException("multiple values of " + tagName);
        }

        return nodeList.item(0).getTextContent();
    }
    private String getFirstValueByTagName(Element element, String tagName, String defaultValue) {
        NodeList nodeList = element.getElementsByTagName(tagName);

        if (nodeList.getLength() >= 1) {
            return nodeList.item(0).getTextContent();
        }

        l.debug("can't find value of {}, using default: {}", tagName, defaultValue);

        return defaultValue;
    }
}
