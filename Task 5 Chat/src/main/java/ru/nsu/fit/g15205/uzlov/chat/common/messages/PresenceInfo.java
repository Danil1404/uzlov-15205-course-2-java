package ru.nsu.fit.g15205.uzlov.chat.common.messages;

import ru.nsu.fit.g15205.uzlov.chat.common.core.VisibleMessage;

import java.time.Instant;

/**
 * Created by Danil on 2017-07-15.
 */
public class PresenceInfo implements VisibleMessage {
    private final Type type;
    private final String from;
    private final String fromID;
    private final Instant time;
    public enum Type {
        LOGIN("присоединился"),
        LOGOUT("отключился");
        private final String content;
        Type(String content) {
            assert content != null;
            this.content = content;
        }
        public String getContent() {
            return content;
        }
    }
    public PresenceInfo(String from, String fromID, Type type, Instant time) {
        assert from != null;
        this.from = from;

        assert fromID != null;
        this.fromID = fromID;

        assert time != null;
        this.time = time;

        assert type != null;
        this.type = type;
    }
    public Type getType() {
        return type;
    }
    @Override
    public String getContent() {
        return type.getContent();
    }
    @Override
    public String getFrom() {
        return from;
    }
    @Override
    public String getFromID() {
        return fromID;
    }
    @Override
    public Instant getTime() {
        return time;
    }
}
