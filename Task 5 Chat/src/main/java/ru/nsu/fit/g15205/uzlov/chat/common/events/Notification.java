package ru.nsu.fit.g15205.uzlov.chat.common.events;

/**
 * Used for messages that should be showed in message list but was created automatically.
 *
 * Examples:
 * LoginNotification, LogoutNotification
 */
public abstract class Notification extends EventTemplate {
}
