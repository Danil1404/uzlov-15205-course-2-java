package ru.nsu.fit.g15205.uzlov.chat.common.events;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import ru.nsu.fit.g15205.uzlov.chat.common.core.TagNamesConstants;
import ru.nsu.fit.g15205.uzlov.chat.common.messages.PresenceInfo;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.time.Instant;

/**
 * Created by Danil on 2017-07-15.
 */
public class LoginNotification extends Notification {
    private final PresenceInfo info;
    public LoginNotification(String from, String fromID, Instant time) {
        this.info = new PresenceInfo(from, fromID, PresenceInfo.Type.LOGIN, time);
    }
    @Override
    public PresenceInfo getContent() {
        return info;
    }

    @Override
    protected Document generateDocument() {
        try {
            Document d = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
            Element e = d.createElement(TagNamesConstants.ROOT_NOTIFICATION);
            e.setAttribute(TagNamesConstants.COMMAND_NAME_IDENTIFIER, TagNamesConstants.COMMAND_NOTIFICATION_LOGIN);
            d.appendChild(e);

            Element n = d.createElement(TagNamesConstants.SUBTAG_NICKNAME);
            n.setTextContent(info.getFrom());
            e.appendChild(n);

            Element f = d.createElement(TagNamesConstants.SUBTAG_FROM_ID);
            f.setTextContent(info.getFromID());
            e.appendChild(f);

            Element t = d.createElement(TagNamesConstants.SUBTAG_TIME);
            t.setTextContent(formatTime(this.info.getTime()));
            e.appendChild(t);

            return d;
        } catch (ParserConfigurationException e) {
            throw new RuntimeException(e);
        }
    }
}
