package ru.nsu.fit.g15205.uzlov.chat.common.network;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;
import ru.nsu.fit.g15205.uzlov.chat.common.core.BytesDocumentSerializer;
import ru.nsu.fit.g15205.uzlov.chat.common.core.SerializingException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * This class should convert org.w3c.dom.Document into XML as byte[] and opposite
 */
public class XMLBytesDocumentSerializer implements BytesDocumentSerializer {
    private static final Logger l = LogManager.getLogger();
    private final ByteArrayOutputStream baos = new ByteArrayOutputStream();
    private final Transformer transformer;
    private final DocumentBuilder builder;
    public XMLBytesDocumentSerializer() throws SerializingException {
        try {
            transformer = TransformerFactory.newInstance().newTransformer();
            builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        } catch (TransformerConfigurationException e) {
            l.error("can't create empty javax.xml.transform.Transformer: {}", e);
            throw new SerializingException("can't create empty javax.xml.transform.Transformer", e);
        } catch (ParserConfigurationException e) {
            l.error("can't crate empty javax.xml.parsers.DocumentBuilder: {}", e);
            throw new RuntimeException("can't crate empty javax.xml.parsers.DocumentBuilder", e);
        }
    }
    public byte[] serialize(Document d) throws SerializingException {
        l.trace("serializing {}", d);
        try {
            baos.reset();
            transformer.reset();
            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            Source source = new DOMSource(d);
            Result result = new StreamResult(baos);
            transformer.transform(source, result);
            return baos.toByteArray();
        } catch (TransformerException e) {
            l.error("can't transform document into bytes: {}", e);
            throw new SerializingException("can't transform document into bytes", e);
        }
    }
    public Document deserialize(byte[] b) throws SerializingException {
        l.trace("deserializing");
        try {
            return builder.parse(new ByteArrayInputStream(b));
        } catch (SAXException | IOException e) {
            l.error("deserializing exception: {}", e);
            throw new SerializingException(e);
        }
    }
}
