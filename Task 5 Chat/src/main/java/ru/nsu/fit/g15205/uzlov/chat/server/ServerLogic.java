package ru.nsu.fit.g15205.uzlov.chat.server;

public interface ServerLogic extends EventHandler {
    void addUser(LinkToUser linkToUser);
}
