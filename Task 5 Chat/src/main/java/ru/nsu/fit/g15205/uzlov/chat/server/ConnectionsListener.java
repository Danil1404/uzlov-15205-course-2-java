package ru.nsu.fit.g15205.uzlov.chat.server;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.nsu.fit.g15205.uzlov.chat.common.core.Protocol;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Created by Danil on 2017-07-10.
 */
public class ConnectionsListener {
    private static final Logger l = LogManager.getLogger();
    private final ConnectionHandler connectionHandler = new ConnectionHandler();
    private final Thread connectionHandlerThread = new Thread(connectionHandler);
    private final ServerLogic serverLogic;
    private final List<Thread> listenerThreads = Collections.synchronizedList(new ArrayList<>());
    public ConnectionsListener(ServerLogic serverLogic) {
        this.connectionHandlerThread.start();
        this.serverLogic = serverLogic;
    }

    public interface ProtocolCreator {
        Protocol create(Socket socket);
    }

    public synchronized void addSocket(ServerSocket socket, ProtocolCreator creator) {
        Thread t = new Thread(() -> {
            l.debug("starting new server socket listener");
            try {
                l.info("waiting for connections");
                while (true) {
                    Socket clientSocket = socket.accept();
                    l.info("new connection: {}", clientSocket);
                    connectionHandler.handleConnection(clientSocket, creator);
                }
            } catch (IOException e) {
                l.error("IOException, stopping socet listener thread, cause: {}", e);
                e.printStackTrace();
            } finally {
                try {
                    socket.close();
                } catch (IOException e) {
                    l.error("error closing server socket: {}", e);
                    e.printStackTrace();
                }
            }
        });
        listenerThreads.add(t);
        l.debug("starting server thread");
        t.start();
    }
    public synchronized void stop() {
        l.info("stopping the server");
        for (Thread t : listenerThreads) {
            t.interrupt();
        }
        connectionHandlerThread.interrupt();
    }
    private class ConnectionHandler implements Runnable {
        private final static int CLIENT_TIMEOUT = 5 * 60 * 1000;
        private final BlockingQueue<Connection> socketQueue = new LinkedBlockingQueue<>();
        ConnectionHandler() {}
        void handleConnection(Socket socket, ProtocolCreator creator) {
            socketQueue.offer(new Connection(socket, creator));
        }
        @Override
        public void run() {
            try {
                l.info("connection handler starts");
                while (true) {
                    Connection con = socketQueue.take();
                    l.info("handling new connection {}", con.s);
                    try {
                        con.s.setSoTimeout(ConnectionHandler.CLIENT_TIMEOUT);
                    } catch (SocketException e) {
                        l.error("error setting timeout: {}", e);
                        e.printStackTrace();
                        continue;
                    }
                    LinkToUser u = new LinkToUser(con.c.create(con.s), serverLogic);
                    serverLogic.addUser(u);
                    l.debug("new LinkToUser object created: {}", u);
                }
            } catch (InterruptedException e) {
                l.info("connection handler interrupted");
            }
        }
        private class Connection {
            final Socket s;
            final ProtocolCreator c;
            Connection(Socket s, ProtocolCreator c) {
                this.s = s;
                this.c = c;
            }
        }
    }
}
