package ru.nsu.fit.g15205.uzlov.chat.client;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.nsu.fit.g15205.uzlov.chat.client.GUI.ClientGUI;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Danil on 2017-07-10.
 */
public class Client {
    private static final Logger l = LogManager.getLogger();
    public static void main(String[] args) {
        l.info("starting new client");
        JFrame frame = new JFrame("Chat");
        frame.setLocationRelativeTo(null);
        frame.setMinimumSize(new Dimension(500, 300));
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        l.debug("generating GUI");
        new ClientGUI().showGUI(frame);

        frame.setVisible(true);
    }
}
