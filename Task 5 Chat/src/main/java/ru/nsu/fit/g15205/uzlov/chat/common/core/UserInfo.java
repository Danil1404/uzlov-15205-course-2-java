package ru.nsu.fit.g15205.uzlov.chat.common.core;

/**
 * Created by Danil on 2017-07-12.
 */
public class UserInfo {
    private final String name;
    private final String client;
    private String uid = null;
    private final String protocolName;
    public UserInfo(String name, String client, String protocolName) {
        assert name != null;
        assert client != null;
        assert protocolName != null;
        this.name = name;
        this.client = client;
        this.protocolName = protocolName;
    }
    public String getName() {
        return name;
    }
    public String getClient() {
        return client;
    }
    public void setUID(String id) {
        assert this.uid == null;
        assert id != null;
        this.uid = id;
    }
    public String getUID() {
        return uid;
    }
    public String getProtocolName() {
        return protocolName;
    }
}
