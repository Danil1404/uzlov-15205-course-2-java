package ru.nsu.fit.g15205.uzlov.chat.common.network;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.nsu.fit.g15205.uzlov.chat.common.core.BytesDocumentSerializer;
import ru.nsu.fit.g15205.uzlov.chat.common.core.Event;
import ru.nsu.fit.g15205.uzlov.chat.common.core.SendTask;
import ru.nsu.fit.g15205.uzlov.chat.common.core.SerializingException;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by Danil on 2017-07-15.
 */
public class CachedSendTask implements SendTask {
    private static final Logger l = LogManager.getLogger();
    private final Event e;
    private final Map<Class, ByteArray> m = new HashMap<>();

    public CachedSendTask(Event e) {
        if (e == null) throw new RuntimeException("Event can't be null");
        this.e = e;
    }

    @Override
    public synchronized byte[] asBytes(BytesDocumentSerializer serializer) throws SerializingException {
        ByteArray ba = m.get(serializer.getClass());
        if (ba == null) {
            ba = m.get(serializer.getClass());
            if (ba == null) {
                l.trace("cached serializing {}", e);
                ba = new ByteArray(serializer.serialize(e.asDocument()));
                m.put(serializer.getClass(), ba);
            }
        }
        return ba.b;
    }
    @Override
    public Event getEvent() {
        return e;
    }
    private class ByteArray {
        final byte[] b;
        ByteArray(byte[] b) {
            this.b = b;
        }
    }
}
