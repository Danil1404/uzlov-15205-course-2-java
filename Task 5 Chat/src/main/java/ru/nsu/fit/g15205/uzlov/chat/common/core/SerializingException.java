package ru.nsu.fit.g15205.uzlov.chat.common.core;

/**
 * Created by Danil on 2017-07-16.
 */
public class SerializingException extends Exception {
    public SerializingException() {}
    public SerializingException(String s) {
        super(s);
    }
    public SerializingException(Throwable t) {
        super(t);
    }
    public SerializingException(String s, Throwable t) {
        super(s, t);
    }
}
