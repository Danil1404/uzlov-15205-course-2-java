package ru.nsu.fit.g15205.uzlov.chat.client;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.nsu.fit.g15205.uzlov.chat.common.core.*;
import ru.nsu.fit.g15205.uzlov.chat.common.events.ConnectionError;
import ru.nsu.fit.g15205.uzlov.chat.common.events.ErrorAnswer;
import ru.nsu.fit.g15205.uzlov.chat.common.events.LoginRequest;
import ru.nsu.fit.g15205.uzlov.chat.common.events.SuccessAnswer;
import ru.nsu.fit.g15205.uzlov.chat.common.network.ProtocolFactory;

import java.net.Socket;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Created by Danil on 2017-07-17.
 */
public class LinkToServer {
    private static final Logger l = LogManager.getLogger();
    private final LinkToServer.Transmitter transmitter;
    private final Thread receiverThread;
    private final Thread transmitterThread;
    private final Protocol protocol;
    private final EventHandler handler;
    private boolean isActive = false;
    private final UserInfo info;
    private EventHandler uidUpdater;
    public LinkToServer(Socket socket, EventHandler visibleMessageHandler, String name) {
        this.protocol = ProtocolFactory.getXMLProtocol(socket);
        this.handler = visibleMessageHandler;
        this.transmitter = new LinkToServer.Transmitter();
        this.info = new UserInfo(name, "Danil's chat", "default");

        this.receiverThread = new Thread(new Receiver());
        this.transmitterThread = new Thread(transmitter);
    }
    public void start() {
        this.receiverThread.start();
        this.transmitterThread.start();
    }
    public void stop() {
        isActive = false;
        receiverThread.interrupt();
        transmitterThread.interrupt();
        protocol.close();
    }
    public void send(Event e) {
        transmitter.send(e);
    }
    public boolean isActive() {
        return isActive;
    }
    public UserInfo getInfo() {
        return info;
    }
    public void setUidUpdater(EventHandler uidUpdater) {
        this.uidUpdater = uidUpdater;
    }
    private class Receiver implements Runnable {
        public void run() {
            try {
                Event e = protocol.getEvent();
                if (e instanceof SuccessAnswer) {
                    if (e.getContent() == null) {
                        l.warn("server didn't send an ID");
                    } else {
                        info.setUID((String) e.getContent());
                        uidUpdater.handleEvent(e);
                    }
                    isActive = true;
                    handler.handleEvent(e);
                } else {
                    l.error("ConnectionsListener's answer to login request is not success");
                    handler.handleEvent(new ErrorAnswer("ConnectionsListener's answer to login request is not success"));
                    stop();
                    return;
                }
                while (true) {
                    e = protocol.getEvent();
                    handler.handleEvent(e);
                }
            } catch (NetworkException | ParsingException e1) {
                isActive = false;
                send(new ErrorAnswer(e1.getMessage()));
                handler.handleEvent(new ConnectionError());
                stop();
                return;
            }
        }
    }

    private class Transmitter implements Runnable {
        private final BlockingQueue<Event> transmitQueue = new LinkedBlockingQueue<>();
        void send(Event e) {
            transmitQueue.offer(e);
        }
        @Override
        public void run() {
            try {
                Event event = new LoginRequest(info);
                protocol.send(event);
                while (true) {
                    event = transmitQueue.take();
                    protocol.send(event);
                }
            } catch (InterruptedException e) {
                isActive = false;
                e.printStackTrace();
            } catch (NetworkException e) {
                isActive = false;
                handler.handleEvent(new ConnectionError());
                stop();
            }
        }
    }
}
