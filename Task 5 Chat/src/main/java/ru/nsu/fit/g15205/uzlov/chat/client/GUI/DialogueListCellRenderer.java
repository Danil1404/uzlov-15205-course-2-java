package ru.nsu.fit.g15205.uzlov.chat.client.GUI;

import ru.nsu.fit.g15205.uzlov.chat.client.Dialogue;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Danil on 2017-07-11.
 */
public class DialogueListCellRenderer extends JPanel implements ListCellRenderer<Dialogue> {
    private final JLabel name;
    private final JTextArea content;
    private final JLabel unread;
    private final static Color UNREAD_BACKGROUND = Color.CYAN;
    DialogueListCellRenderer(Dimension size) {
        this.setLayout(new BorderLayout());

        name = new JLabel();
        name.setFont(name.getFont().deriveFont(16f));

        content = new JTextArea();
        makeLookLikeLabel(content);

        unread = new JLabel();
        unread.setBackground(UNREAD_BACKGROUND);

        JPanel dialogueHeader = new JPanel(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.weightx = 1;
        c.ipadx = 5;
        c.anchor = GridBagConstraints.PAGE_START;
        dialogueHeader.add(name, c);
        c.weightx = 0;
        c.anchor = GridBagConstraints.PAGE_END;
        c.insets = new Insets(1, 1, 1, 20);
        dialogueHeader.add(unread, c);

        this.add(dialogueHeader, BorderLayout.NORTH);
        this.add(content, BorderLayout.CENTER);

        this.setPreferredSize(size);
    }
    public static void makeLookLikeLabel(JTextArea e) {
        e.setWrapStyleWord(true);
        e.setLineWrap(true);
        e.setOpaque(false);
        e.setEditable(false);
        e.setFocusable(false);
        e.setBackground(UIManager.getColor("Label.background"));
        e.setFont(UIManager.getFont("Label.font"));
        e.setBorder(UIManager.getBorder("Label.border"));
    }
    private final static int NAME_LENGTH = 23;
    private final static int MSG_LENGTH = 140;
    @Override
    public Component getListCellRendererComponent(JList<? extends Dialogue> list, Dialogue value, int index, boolean isSelected, boolean cellHasFocus) {
        String cut;
        String s;

        s = value.getName();
        if (s.length() > NAME_LENGTH) {
            cut = s.substring(0, NAME_LENGTH - 3) + "...";
            this.setToolTipText(s);
        }
        else {
            cut = s;
            this.setToolTipText(null);
        }
        name.setText(cut);

        s = value.getLastMessageContent();
        if (s.length() > MSG_LENGTH)
            cut = s.substring(0, MSG_LENGTH - 3) + "...";
        else
            cut = s;
        content.setText(cut);

        Integer unreadCount = value.getUnreadCount();
        if (unreadCount != 0) {
            unread.setOpaque(true);
            unread.setText(unreadCount.toString());
        }
        else {
            unread.setOpaque(false);
            unread.setText(null);
        }

        if (isSelected) {
            this.setBackground(list.getSelectionBackground());
            this.setForeground(list.getSelectionForeground());
        } else {
            this.setBackground(list.getBackground());
            this.setForeground(list.getForeground());
        }
        return this;
    }
}
