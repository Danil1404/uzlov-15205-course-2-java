package ru.nsu.fit.g15205.uzlov.chat.common.core;

/**
 * Created by Danil on 2017-07-16.
 */
public class NetworkException extends Exception {
    public NetworkException() {}
    public NetworkException(String s) {
        super(s);
    }
    public NetworkException(Throwable t) {
        super(t);
    }
    public NetworkException(String s, Throwable t) {
        super(s, t);
    }
}
