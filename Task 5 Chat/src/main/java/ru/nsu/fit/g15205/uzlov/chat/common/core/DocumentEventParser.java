package ru.nsu.fit.g15205.uzlov.chat.common.core;

import org.w3c.dom.Document;

public interface DocumentEventParser {
    Event parse(Document d) throws ParsingException;
}
