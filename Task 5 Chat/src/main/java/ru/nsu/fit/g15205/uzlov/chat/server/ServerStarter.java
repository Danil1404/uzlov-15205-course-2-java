package ru.nsu.fit.g15205.uzlov.chat.server;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.nsu.fit.g15205.uzlov.chat.common.network.ProtocolFactory;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.Scanner;

/**
 * Created by Danil on 2017-07-15.
 */
class ServerStarter {
    private static final Logger l = LogManager.getLogger();
    public static void main(String[] args){
        l.info("starting new connectionsListener with args {}", (Object) args);
        int xmlPort = 65003;
        int objStreamPort = 65004;
        if (args.length == 2) {
            l.info("2 args found, trying to parse as port numbers");
            try {
                xmlPort = Integer.parseInt(args[0]);
            } catch (NumberFormatException e) {
                l.fatal("can't parse xml port number: {}", e);
                System.exit(10);
            }
            try {
                objStreamPort = Integer.parseInt(args[1]);
            } catch (NumberFormatException e) {
                l.fatal("can't parse object stream port number: {}", e);
                System.exit(11);
            }
        } else {
            l.info("args count is not 2, using standard ports (" + xmlPort + " and " + objStreamPort + ")");
        }

        ServerSocket xmlSocket = null;
        try {
            xmlSocket = new ServerSocket(xmlPort);
        } catch (IOException e) {
            l.fatal("cant open socket on xmlPort " + xmlPort + ", cause: {}", e);
            e.printStackTrace();
            System.exit(1);
        }
        ServerSocket objectStreamSocket = null;
        try {
            objectStreamSocket = new ServerSocket(objStreamPort);
        } catch (IOException e) {
            l.fatal("cant open socket on object stream port " + objStreamPort + ", cause: {}", e);
            e.printStackTrace();
            System.exit(2);
        }

        StandardServerLogic logic = new StandardServerLogic();
        Thread logicThread = new Thread(logic);
        logicThread.start();

        ConnectionsListener connectionsListener = new ConnectionsListener(logic);
        connectionsListener.addSocket(xmlSocket, ProtocolFactory::getXMLProtocol);
        connectionsListener.addSocket(objectStreamSocket, ProtocolFactory::getObjectStreamProtocol);

        Scanner scanner = new Scanner(System.in);
        l.debug("waiting for exit command");
        while (true) {
            String string = scanner.nextLine();
            if (string.equals("exit")) {
                l.info("received exit command");
                l.debug("stopping connectionsListener thread");
                connectionsListener.stop();
                l.debug("interrupting logic thread");
                logicThread.interrupt();
                return;
            }
        }
    }
}
