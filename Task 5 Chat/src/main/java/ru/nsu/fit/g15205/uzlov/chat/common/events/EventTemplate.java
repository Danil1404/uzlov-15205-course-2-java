package ru.nsu.fit.g15205.uzlov.chat.common.events;

import org.w3c.dom.Document;
import ru.nsu.fit.g15205.uzlov.chat.common.core.Event;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.time.Instant;
import java.time.format.DateTimeFormatter;

/**
 * Created by Danil on 2017-07-15.
 */
public abstract class EventTemplate implements Event {
    private Document d = null;
    @Override
    public final Document asDocument() {
        if (d != null)
            return d;
        synchronized (this) {
            if (d == null) {
                d = generateDocument();
            }
        }
        return d;
    }
    protected abstract Document generateDocument();
    protected final String formatTime(Instant time) {
        return DateTimeFormatter.ISO_INSTANT.format(time);
    }
}
