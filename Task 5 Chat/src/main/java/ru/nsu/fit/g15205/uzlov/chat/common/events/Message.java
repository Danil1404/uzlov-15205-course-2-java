package ru.nsu.fit.g15205.uzlov.chat.common.events;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import ru.nsu.fit.g15205.uzlov.chat.common.core.TagNamesConstants;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

/**
 * Created by Danil on 2017-07-15.
 */
public class Message extends EventTemplate {
    private final String s;
    private final String id;
    public Message(String s, String id) {
        assert s != null;
        assert id != null;
        this.s = s;
        this.id = id;
    }
    @Override
    public String getContent() {
        return s;
    }
    @Override
    protected Document generateDocument() {
        try {
            Document d = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
            Element c = d.createElement(TagNamesConstants.ROOT_REQUEST);
            c.setAttribute(TagNamesConstants.COMMAND_NAME_IDENTIFIER, TagNamesConstants.COMMAND_REQUEST_SEND_MESSAGE);
            d.appendChild(c);

            Element m = d.createElement(TagNamesConstants.SUBTAG_MESSAGE_TEXT);
            m.setTextContent(s);
            c.appendChild(m);

            Element usid = d.createElement(TagNamesConstants.SUBTAG_UID);
            usid.setTextContent(id);
            c.appendChild(usid);

            return d;
        } catch (ParserConfigurationException e) {
            throw new RuntimeException(e);
        }
    }
}
