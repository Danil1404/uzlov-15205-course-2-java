package ru.nsu.fit.g15205.uzlov.flow.game;

import ru.nsu.fit.g15205.uzlov.flow.game.core.*;

import java.awt.*;

/**
 * Created by Danil on 2017-05-23.
 */
public class FieldGeneratorStandard implements FieldGenerator{
    private GameField gf;
    private int m[][];
    private boolean canMakeMove(Dimension initialPoint, int type, Direction direction) {
        int x = initialPoint.width + direction.getDx();
        int y = initialPoint.height + direction.getDy();
        return gf.isOnField(x, y) && m[x][y] == 0 && !isSelfNeighbour(x, y, type, direction.opposite());
    }
    private boolean isSelfNeighbour(int x, int y, int type, Direction direction) {
        for (int i = 0; i < Direction.values().length; i++) {
            Direction dtmp = Direction.values()[i];
            int x1 = x + dtmp.getDx();
            int y1 = y + dtmp.getDy();
            if (direction != dtmp && gf.isOnField(x1, y1) && m[x1][y1] == type)
                return true;
        }
        return false;
    }
    @Override
    public GameField generateNewField(int width, int height, int colorsCount) {
        gf = new GameFieldStandard(width, height);
        if (colorsCount  < 3 || colorsCount > MyColor.values().length)
            return null;
        m = new int[width][];
        for (int i = 0; i < width; i++)
            m[i] = new int[height];
        int counts[] = new int[colorsCount + 1];
        counts[0] = width * height;
        Dimension d1 = new Dimension();
        Dimension d2 = new Dimension();
        for (int i = 1; i <= colorsCount; i++) {
            //  Выбираем случайную клетку
            d1.width = (int)(Math.random() * width);
            d1.height = (int)(Math.random() * height);
            //  Если выбранная клетка уже что-то содержит, то ищем ближайшую (справа), на которой ничего нет
            while(m[d1.width][d1.height] != 0) {
                if (d1.width < width - 1) {
                    d1.width++;
                } else {
                    d1.width = 0;
                    d1.height = d1.height < height - 1 ? d1.height + 1 : 0;
                }
            }
            //  d2 - рабочая переменная. d1 - начало маршрута
            d2.setSize(d1);
            m[d2.width][d2.height] = i;
            counts[0]--;
            counts[i]++;
            Direction d = Direction.randomDirection();
            int x, y;
            boolean dt = false;
            //  Цикл для построения "случайного" маршрута от выбранной клетки
            while (true) {
                int tmp = 0;
                //  Проверяем, есть ли хотя бы одна соседняя ячейка, куда можно направить линию
                for (int i1 = 0; i1 < Direction.values().length; i1++)
                    if (canMakeMove(d2, i, Direction.values()[i1]))
                        tmp++;
                if (tmp == 0)
                    break;
                if (!canMakeMove(d2, i, d) || Math.random() < 0.15)
                    d = dt ? d.right() : d.left();
                x = d2.width + d.getDx();
                y = d2.height + d.getDy();
                if (canMakeMove(d2, i, d)) {
                    m[x][y] = i;
                    d2.setSize(x, y);
                    counts[0]--;
                    counts[i]++;
                    dt = Math.random() < 0.5 && !dt;
                }
            }
            boolean flag = false;
            if (counts[i] == 1) {
                byte b = rebalance1(d1);
                switch (b) {
                    case 0:
                        System.out.println();
                        return generateNewField(width, height, colorsCount);
                    case 1:
                        flag = true;
                        System.out.println("!");
                        break;
                    default:
                        break;
                }
            }
            boolean r = true;
//            if (counts[i] == 2)
//                r = rebalance2(d1, d2);
            //  Добавляем начало и конец маршрута в качестве источников
            if (counts[i] > 1 && r)
                gf.setSource(MyColor.values()[i - 1], d1, d2);
            if (counts[0] == 0)
                break;
            if (flag)
                i--;
        }
        if (counts[0] != 0)
            System.out.println("Не хватило цветов");
        myPrintArray(m);
        return gf;
    }
    private void myPrintArray(int[][] a) {
        for (int i = 0; i < a[0].length; i++) {
            for (int j = 0; j < a.length; j++)
                System.out.print(a[j][i]);
            System.out.println();
        }
        System.out.println();
    }
    private byte rebalance1(Dimension d) {
        for (int i = 0; i < Direction.values().length; i++) {
            Direction dtmp = Direction.values()[i];
            int x = d.width + dtmp.getDx();
            int y = d.height + dtmp.getDy();
            Dimension nd = new Dimension(x, y);
            int tmp = m[d.width][d.height];
            m[d.width][d.height] = 0;
            if (gf.isOnField(x, y) && canMakeMove(nd, m[x][y], dtmp.opposite())) {
                m[d.width][d.height] = tmp;
                Cell c = gf.getCell(nd);
                if (c.isSource()) {
                    //  Это случай, если мы можем просто продолжить какую-то соседнюю линию
                    gf.changeSource(c.getColor(), d, gf.getSecondSource(nd));
                    m[d.width][d.height] = m[nd.width][nd.height];
                    return 1;
                }
            }
            m[d.width][d.height] = tmp;
        }
        for (int i = 0; i < Direction.values().length; i++) {
            Direction dtmp = Direction.values()[i];
            int x = d.width + dtmp.getDx();
            int y = d.height + dtmp.getDy();
            Dimension nd = new Dimension(x, y);
            int tmp = m[d.width][d.height];
            m[d.width][d.height] = 0;
            if (gf.isOnField(x, y) && canMakeMove(nd, m[x][y], dtmp.opposite())) {
                m[d.width][d.height] = tmp;
                if (!isInMiddleOfThreeLengthLine(nd)) {
                    //  Здесь мы рвем сосседнюю линию на две части
                    Dimension dt = halfRecolor(nd, m[d.width][d.height]);
                    gf.setSource(MyColor.values()[m[d.width][d.height] - 1], d, dt);
                    return 2;
                }
            }
            m[d.width][d.height] = tmp;
        }
        //  Следующие строки - только мое предположение
        //  Если цикл выше не смог ничего сделать, то у нас такой случай:
        //  111     112
        //  1.1     1.3
        //  000     433

        //  ..........
        //  Я решил просто перегененировать до тех пор, пока не получится без таких случаев
        return 0;
    }
    private boolean isInMiddleOfThreeLengthLine(Dimension d) {
        Dimension[] md = gf.getSourcePair(MyColor.values()[m[d.width][d.height] - 1]);
        return (Math.abs(d.width - md[0].width) + Math.abs(d.height - md[0].height)) == 1 &&
                (Math.abs(d.width - md[1].width) + Math.abs(d.height - md[1].height)) == 1;
    }
    private Dimension halfRecolor(Dimension tearPoint, int newType) {
        Dimension[] md = gf.getSourcePair(MyColor.values()[m[tearPoint.width][tearPoint.height] - 1]);
        Dimension d = Math.abs(tearPoint.width - md[0].width) + Math.abs(tearPoint.height - md[0].height) <
                Math.abs(tearPoint.width - md[1].width) + Math.abs(tearPoint.height - md[1].height) ? (Dimension)md[0].clone() : (Dimension)md[1].clone();
        int type = m[d.width][d.height];
        while (!d.equals(tearPoint)) {
            m[d.width][d.height] = newType;
            for (int i = 0; i < Direction.values().length; i++) {
                Direction dtmp = Direction.values()[i];
                int x = d.width + dtmp.getDx();
                int y = d.height + dtmp.getDy();
                if (gf.isOnField(x, y) && m[x][y] == type) {
                    d.setSize(x, y);
                    break;
                }
            }
        }
        m[d.width][d.height] = newType;
        Dimension d1 = new Dimension();
        for (int i = 0; i < Direction.values().length; i++) {
            Direction dtmp = Direction.values()[i];
            int x = tearPoint.width + dtmp.getDx();
            int y = tearPoint.height + dtmp.getDy();
            if (gf.isOnField(x, y) && m[x][y] == type) {
                d1.setSize(x, y);
                int a;
                break;
            }
        }
        if (Math.abs(tearPoint.width - md[0].width) + Math.abs(tearPoint.height - md[0].height) <
                Math.abs(tearPoint.width - md[1].width) + Math.abs(tearPoint.height - md[1].height)) {
            gf.changeSource(MyColor.values()[type - 1], md[1], d1);
            return md[0];
        }
        else {
            gf.changeSource(MyColor.values()[type - 1], md[0], d1);
            return md[1];
        }
    }

    //  По идее код снизу должен немного уменьшать количество пар источников, расположенных на соседних клетках,
    //  но на практике он глючит при каждом срабатывании
//    private boolean rebalance2(Dimension d1, Dimension d2) {
//        Dimension nd1 = null, nd2 = null;
//        boolean b1 = false, b2 = false;
//        for (int i = 0; i < Direction.values().length; i++) {
//            Direction dtmp = Direction.values()[i];
//            int x = d1.width + dtmp.getDx();
//            int y = d1.height + dtmp.getDy();
//            nd1 = new Dimension(x, y);
//            int tmp = m[d1.width][d1.height];
//            m[d1.width][d1.height] = 0;
//            if (!(x != d2.width && y != d2.height) && gf.isOnField(x, y) && canMakeMove(nd1, m[x][y], dtmp.opposite())) {
//                m[d1.width][d1.height] = tmp;
//                Cell c = gf.getCell(nd1);
//                if (c.isSource()) {
//                    //  Это случай, если мы можем просто продолжить какую-то соседнюю линию
//                    break;
//                }
//            }
//            m[d1.width][d1.height] = tmp;
//        }
//        for (int i = 0; i < Direction.values().length; i++) {
//            Direction dtmp = Direction.values()[i];
//            int x = d2.width + dtmp.getDx();
//            int y = d2.height + dtmp.getDy();
//            nd2 = new Dimension(x, y);
//            int tmp = m[d2.width][d2.height];
//            m[d2.width][d2.height] = 0;
//            if (!(x != d1.width && y != d1.height) && gf.isOnField(x, y) && canMakeMove(nd2, m[x][y], dtmp.opposite())) {
//                m[d2.width][d2.height] = tmp;
//                Cell c = gf.getCell(nd2);
//                int t;
//                if (c.isSource()) {
//                    //  Это случай, если мы можем просто продолжить какую-то соседнюю линию
//                    break;
//                }
//            }
//            m[d2.width][d2.height] = tmp;
//        }
//        if (nd1 != null && nd2 != null) {
//            gf.changeSource(gf.getCell(nd1).getColor(), d1, gf.getSecondSource(nd1));
//            m[d1.width][d1.height] = m[nd1.width][nd1.height];
//            gf.changeSource(gf.getCell(nd2).getColor(), d2, gf.getSecondSource(nd2));
//            m[d2.width][d2.height] = m[nd2.width][nd2.height];
//            return false;
//        }
//        return true;
//    }
}
