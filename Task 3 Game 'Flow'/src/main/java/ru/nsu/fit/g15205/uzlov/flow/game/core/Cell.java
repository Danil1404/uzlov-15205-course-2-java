package ru.nsu.fit.g15205.uzlov.flow.game.core;

/**
 * Created by Danil on 28.03.2017.
 */
public interface Cell {
    boolean isSource();
    MyColor getColor();
    void setIn(MyColor color, Direction in);
    void setOut(Direction out);
    Direction getIn();
    Direction getOut();
}
