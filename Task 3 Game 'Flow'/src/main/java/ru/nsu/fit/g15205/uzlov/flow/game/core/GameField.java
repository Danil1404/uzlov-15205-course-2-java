package ru.nsu.fit.g15205.uzlov.flow.game.core;

import java.awt.*;

/**
 * Created by Danil on 28.03.2017.
 */
public interface GameField {
    int getWidth();
    int getHeight();
    Dimension[] getSourcePair(MyColor color);
    Dimension getSecondSource(Dimension source);
    Cell getCell(Dimension point);
    Cell getCell(int x, int y);
    void setSource(MyColor color, Dimension point1, Dimension point2);
    void changeSource(MyColor color, Dimension point1, Dimension point2);
    boolean isOnField(int x, int y);
    boolean isOnField(Dimension point);
    boolean isPossibleMove(Dimension initialPoint, Dimension movePoint);
    boolean isFull();
}
