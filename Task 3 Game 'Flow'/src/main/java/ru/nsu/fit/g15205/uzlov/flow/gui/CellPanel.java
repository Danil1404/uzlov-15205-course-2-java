package ru.nsu.fit.g15205.uzlov.flow.gui;

import ru.nsu.fit.g15205.uzlov.flow.game.core.*;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Danil on 2017-05-07.
 */
class CellPanel extends JPanel {
    private Cell cell;
    private static Dimension d = new Dimension(100,100);
    private static final Color backColor = Color.BLACK;
    private static final Color selectedColor = Color.GRAY;
    CellPanel(Cell cell) {
        this.cell = cell;
        this.setPreferredSize(d);
        this.setBackground(backColor);
    }
    @Override
    protected final void paintComponent(Graphics g) {
        super.paintComponent(g);
        Rectangle r = g.getClipBounds();
        int w = r.width;
        MyColor mc = cell.getColor();
        if (mc != null) {
            g.setColor(mc.getAWTColor());
            if (cell.isSource()) {
                g.fillOval((int) (w * 0.2), (int) (w * 0.2), (int) (w * 0.6), (int) (w * 0.6));
            }
            if (cell.getIn() != null)
                drawPipe(g, cell.getIn());
            if (cell.getOut() != null)
                drawPipe(g, cell.getOut());
        }
    }
    void setSelected(boolean selected) {
        if (selected)
            this.setBackground(selectedColor);
        else
            this.setBackground(backColor);
    }
    private void drawPipe(Graphics g, Direction d) {
        Rectangle r = g.getClipBounds();
        int w = r.width;
        switch (d) {
            case UP:
                g.fillRect((int)(w * 0.4), 0, (int)(w * 0.2), (int)(w * 0.2) + (int)(w * 0.4));
                break;
            case DOWN:
                g.fillRect((int)(w * 0.4), (int)(w * 0.4), (int)(w * 0.2), (int)(w * 0.2) + (int)(w * 0.4));
                break;
            case LEFT:
                g.fillRect(0, (int)(w * 0.4), (int)(w * 0.2) + (int)(w * 0.4), (int)(w * 0.2));
                break;
            case RIGHT:
                g.fillRect((int)(w * 0.4), (int)(w * 0.4), (int)(w * 0.2) + (int)(w * 0.4), (int)(w * 0.2));
                break;
        }
    }
    public void setCell(Cell cell) {
        this.cell = cell;
    }
    @Override
    public Dimension getPreferredSize() {
        return d;
    }
}
