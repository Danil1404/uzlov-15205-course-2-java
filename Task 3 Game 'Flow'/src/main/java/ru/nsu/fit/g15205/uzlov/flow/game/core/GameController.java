package ru.nsu.fit.g15205.uzlov.flow.game.core;

import java.awt.*;

/**
 * Created by Danil on 2017-05-23.
 */
public interface GameController {
    GameField getField();
    GameField setField(GameField gameField);
    GameField nextField();
    GameField nextField(int width, int height);
    boolean isWin();
    boolean selectCell(Dimension d);
    void unselectCell();
    boolean makeMove(Dimension moveCoordinate);
}
