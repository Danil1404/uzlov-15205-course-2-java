package ru.nsu.fit.g15205.uzlov.flow.game.core;

import java.awt.*;


/**
 * Created by Danil on 02.04.2017.
 */
public enum MyColor {
    RED(Color.RED),
    GREEN(Color.GREEN),
    BLUE(Color.BLUE),
    CYAN(Color.CYAN),
    DARK_GRAY(Color.DARK_GRAY),
    GRAY(Color.GRAY),
    LIGHT_GRAY(Color.LIGHT_GRAY),
    MAGENTA(Color.MAGENTA),
    ORANGE(Color.ORANGE),
    PINK(Color.PINK),
    WHITE(Color.WHITE),
    YELLOW(Color.YELLOW),
    c1(new Color(10,150,200)),
    c2(new Color(10,150,150)),
    c3(new Color(150,0,200)),
    c4(new Color(150,200,250)),
    c5(new Color(50,80,40)),
    c6(new Color(150,200,50)),
    c7(new Color(150,100,50))
    ;

    public final Color awtColor;

    MyColor(Color c) {
        awtColor = c;
    }

    public Color getAWTColor() {
        return awtColor;
    }
}
