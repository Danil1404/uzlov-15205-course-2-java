package ru.nsu.fit.g15205.uzlov.flow;


import ru.nsu.fit.g15205.uzlov.flow.game.FieldGeneratorStandard;
import ru.nsu.fit.g15205.uzlov.flow.game.GameControllerStandard;
import ru.nsu.fit.g15205.uzlov.flow.gui.PlayGUI;

import javax.swing.*;
import java.awt.*;

public class App {
    public static void main( String[] args ) {
        final JFrame frame = new JFrame("Flow The Game");
        frame.setLocationRelativeTo(null);
        frame.setMinimumSize(new Dimension(400, 450));
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        PlayGUI pg = new PlayGUI(new GameControllerStandard(new FieldGeneratorStandard(), 5, 5));
        pg.drawPlayGUI(frame);

        frame.setVisible(true);

    }
}
