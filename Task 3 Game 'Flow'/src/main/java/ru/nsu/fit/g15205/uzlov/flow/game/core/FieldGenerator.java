package ru.nsu.fit.g15205.uzlov.flow.game.core;

/**
 * Created by Danil on 28.03.2017.
 */
public interface FieldGenerator {
    GameField generateNewField(int width, int height, int colorsCount);
}
