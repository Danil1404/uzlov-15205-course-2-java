package ru.nsu.fit.g15205.uzlov.flow.game;

import ru.nsu.fit.g15205.uzlov.flow.game.core.Cell;
import ru.nsu.fit.g15205.uzlov.flow.game.core.MyColor;
import ru.nsu.fit.g15205.uzlov.flow.game.core.Direction;

/**
 * Created by Danil on 02.04.2017.
 */
public class CellStandard implements Cell {
    private MyColor color;
    private Direction in;
    private Direction out;

    CellStandard() {}

    @Override
    public boolean isSource() {
        return false;
    }

    @Override
    public MyColor getColor() {
        return color;
    }

    @Override
    public void setIn(MyColor color, Direction in) {
        if (this.out != null && this.out == in)
            throw new RuntimeException("Controller error: in == out");
        this.color = color;
        this.in = in;
    }

    @Override
    public void setOut(Direction out) {
        if (this.in != null && this.in == out)
            throw new RuntimeException("Controller error: in == out");
        this.out = out;
    }

    @Override
    public Direction getIn() {
        return in;
    }

    @Override
    public Direction getOut() {
        return out;
    }
}
