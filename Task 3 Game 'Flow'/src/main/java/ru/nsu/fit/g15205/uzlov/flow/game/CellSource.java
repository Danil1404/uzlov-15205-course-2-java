package ru.nsu.fit.g15205.uzlov.flow.game;

import ru.nsu.fit.g15205.uzlov.flow.game.core.Cell;
import ru.nsu.fit.g15205.uzlov.flow.game.core.Direction;
import ru.nsu.fit.g15205.uzlov.flow.game.core.MyColor;

/**
 * Created by Danil on 2017-05-09.
 */
public class CellSource  implements Cell {
    private final MyColor color;
    private Direction in;
    private Direction out;

    CellSource(MyColor color) {
        this.color = color;
    }

    @Override
    public boolean isSource() {
        return true;
    }

    @Override
    public MyColor getColor() {
        return color;
    }

    @Override
    public void setIn(MyColor color, Direction in) {
        if (color != null && this.color != color)
            throw new RuntimeException("Ошибка в логике контроллера - нельзя перекрасить источник " + this.color + " в " + color);
        this.in = in;
        out = null;
    }

    @Override
    public void setOut(Direction out) {
        this.out = out;
        in = null;
    }

    @Override
    public Direction getIn() {
        return in;
    }

    @Override
    public Direction getOut() {
        return out;
    }
}

