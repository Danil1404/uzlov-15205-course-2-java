package ru.nsu.fit.g15205.uzlov.flow.gui;

import ru.nsu.fit.g15205.uzlov.flow.game.core.GameController;
import ru.nsu.fit.g15205.uzlov.flow.game.core.GameField;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.*;


/**
 * Created by Danil on 28.03.2017.
 */
public class PlayGUI {
    private FieldPanel fieldPanel;
    private JPanel mainPlayGUIPanel;
    private CellPanel[][] fieldCellPanels;
    private GameController gc;
    private GameField gameField;

    private boolean mousePressed = false;
    private Dimension pressedCoordinate = null;
    private Dimension lockedCoordinate = null;
    private class panelMouseListener implements MouseListener {
        @Override
        public void mouseClicked(MouseEvent e) {}
        @Override
        public void mousePressed(MouseEvent e) {}
        @Override
        public void mouseReleased(MouseEvent e) {}
        @Override
        public void mouseEntered(MouseEvent e) {
            mousePressed = false;
            if (lockedCoordinate != null)
                fieldCellPanels[lockedCoordinate.height][lockedCoordinate.width].setSelected(false);
            lockedCoordinate = null;
            gc.unselectCell();
        }
        @Override
        public void mouseExited(MouseEvent e) {}
    }
    private class cellMouseListener implements MouseListener {
        Dimension p;
        cellMouseListener(int x, int y) {
            p = new Dimension(x, y);
        }
        @Override
        public void mouseClicked(MouseEvent e) {}
        @Override
        public void mousePressed(MouseEvent e) {
            if (gameField.getCell(p).getColor() != null) {
                if (gc.selectCell(p)) {
                    mousePressed = true;
                    fieldPanel.repaint();
                }
            }
        }
        @Override
        public void mouseReleased(MouseEvent e) {
            mousePressed = false;
            if (lockedCoordinate != null)
                fieldCellPanels[lockedCoordinate.height][lockedCoordinate.width].setSelected(false);
            lockedCoordinate = null;
            gc.unselectCell();
        }
        @Override
        public void mouseEntered(MouseEvent e) {
            if (mousePressed) {
                if (gc.makeMove(p)) {
                    fieldCellPanels[p.height][p.width].setSelected(true);
                    if (lockedCoordinate != null)
                        fieldCellPanels[lockedCoordinate.height][lockedCoordinate.width].setSelected(false);
                    lockedCoordinate = null;
                    if (gc.isWin()) {
                        mousePressed = false;
                        gc.unselectCell();
                        fieldPanel.repaint();
                        String[] options = {"Да", "Нет", "Выход"};
                        int answer = JOptionPane.showOptionDialog(mainPlayGUIPanel, "Вы хотите начать новую игру?", "Победа!", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
                        switch (answer) {
                            case 0: {
                                nextLevel();
                                break;
                            }
                            case 1: {
                                break;
                            }
                            case 2: {
                                System.exit(0);
                            }
                        }
                    }
                } else {
                    if (lockedCoordinate == null)
                        lockedCoordinate = pressedCoordinate;
                    fieldCellPanels[lockedCoordinate.height][lockedCoordinate.width].setSelected(true);
                }
                fieldPanel.repaint();
            } else {
                fieldCellPanels[p.height][p.width].setSelected(true);
            }
        }
        @Override
        public void mouseExited(MouseEvent e) {
            fieldCellPanels[p.height][p.width].setSelected(false);
            pressedCoordinate = p;
        }
    }
    //  Контейнер клеток, предназначенный для того, чтобы быть всегда квадратным.
    //  Со своей работой пока не справляется... :(
    private class FieldPanel extends JPanel {
//        private int width;
//        private int height;
        private double fieldAspectRatio;
        //  d - для getPreferredSize(), просто чтобы не создавать каждый раз новый объект для рамера
        private Dimension d;
        GridLayout gl;
        FieldPanel(GameField field) {
            setDimensions(field.getWidth(), field.getHeight());
            gl = new GridLayout();
            setLayout(gl);
            d = new Dimension();
            setField(field);
        }
        void setDimensions(int width, int height) {
//            this.width = width;
//            this.height = height;
            fieldAspectRatio = ((double)width) / height;
        }
        public Dimension getPreferredSize() {
            Container c = getParent();
            int w = c.getWidth();
            int h = c.getHeight();
            double rar = ((double)w) / h;
            if (fieldAspectRatio >= rar) {
                d.setSize(w, w / fieldAspectRatio);
            } else {
                d.setSize(fieldAspectRatio * h, h);
            }
            return d;
        }
        void setField(GameField field) {
            if (field == null)
                return;
            int w = gl.getColumns();
            int h = gl.getRows();
            int width = field.getWidth();
            int height = field.getHeight();
            if (w != width || h != height) {
                setDimensions(width, height);
                this.removeAll();
                gl.setColumns(width);
                gl.setRows(height);
                fieldCellPanels = new CellPanel[height][];
                final Border b = BorderFactory.createLineBorder(Color.DARK_GRAY);
                for (int y = 0; y < height; y++) {
                    fieldCellPanels[y] = new CellPanel[width];
                    for (int x = 0; x < width; x++) {
                        CellPanel tmp = new CellPanel(field.getCell(x, y));
                        tmp.setBackground(Color.BLACK);
                        tmp.setBorder(b);
                        tmp.addMouseListener(new cellMouseListener(x, y));
                        this.add(tmp);
                        fieldCellPanels[y][x] = tmp;
                    }
                }
            } else {
                for (int x = 0; x < width; x++)
                    for (int y = 0; y < height; y++)
                        fieldCellPanels[y][x].setCell(field.getCell(x, y));
            }

        }
    }
    public GameField nextLevel() {
        return nextLevel(gameField.getWidth(), gameField.getHeight());
    }
    public GameField nextLevel(int width, int height) {
        GameField gf = gc.nextField(width, height);
        if (gf == null)
            return null;
        gameField = gf;
        fieldPanel.setField(gameField);
        fieldPanel.repaint();
        return gf;
    }
    public PlayGUI(GameController gameController) {
        gameField = gameController.getField();
        fieldPanel = new FieldPanel(gameField);
        mainPlayGUIPanel = new JPanel();
        this.gc = gameController;

        mainPlayGUIPanel.setLayout(new BoxLayout(mainPlayGUIPanel, BoxLayout.X_AXIS));
        //  Если добавлять просто на основную панель, то fieldPanel не остается квадратной
        JPanel tmp = new JPanel(new GridBagLayout());
        tmp.add(fieldPanel);
        mainPlayGUIPanel.add(tmp);

        fieldPanel.addMouseListener(new panelMouseListener());
        //fieldPanel.setField(gameField);

    }
    public void drawPlayGUI(JFrame mainWindowFrame) {

        JMenuBar menuBar = new JMenuBar();
        JMenu menu = new JMenu("Меню");
        //menu.setMnemonic(KeyEvent.VK_ALT);
        JMenuItem menuItemNew = new JMenuItem("Новая игра", KeyEvent.VK_F2);
        menuItemNew.addActionListener(e -> nextLevel());
        menuItemNew.setAccelerator(KeyStroke.getKeyStroke("F2"));

        JMenuItem menuItemNewParam = new JMenuItem("Новая игра...");
        menuItemNewParam.addActionListener(e -> {
            JTextField wField = new JTextField(((Integer)gameField.getWidth()).toString(), 5);
            JTextField hField = new JTextField(((Integer)gameField.getHeight()).toString(), 5);
            JPanel myPanel = new JPanel();
            myPanel.add(new JLabel("Ширина (5-20):"));
            myPanel.add(wField);
            myPanel.add(Box.createHorizontalStrut(15));
            myPanel.add(new JLabel("Высота (5-20):"));
            myPanel.add(hField);
            int result = JOptionPane.showConfirmDialog(mainWindowFrame, myPanel,
                    "New game", JOptionPane.OK_CANCEL_OPTION);
            if (result == 0) {
                int width;
                int height;
                try { width = Integer.parseInt(wField.getText()); }
                catch (NumberFormatException nfe) { JOptionPane.showMessageDialog(mainWindowFrame, "\"" + wField.getText() + "\" is not a number.", "Error", JOptionPane.ERROR_MESSAGE); return; }
                try { height = Integer.parseInt(hField.getText()); }
                catch (NumberFormatException nfe) { JOptionPane.showMessageDialog(mainWindowFrame, "\"" + hField.getText() + "\" is not a number.", "Error", JOptionPane.ERROR_MESSAGE); return; }
                if (width > 20 || height > 20) {
                    JOptionPane.showMessageDialog(mainWindowFrame, "Can't set field size more than 20 in any dimension.", "Error", JOptionPane.ERROR_MESSAGE);
                    return;
                }
                if (nextLevel(width, height) == null) {
                    JOptionPane.showMessageDialog(mainWindowFrame, "Can't set this field size.", "Error", JOptionPane.ERROR_MESSAGE);
                    return;
                }

                mainWindowFrame.pack();
                mainWindowFrame.getContentPane().repaint();
            }
        });

        JMenuItem menuItemExit = new JMenuItem("Выйти");
        menuItemExit.addActionListener(e -> System.exit(0));

        mainWindowFrame.setJMenuBar(menuBar);

        menuBar.add(menu);
        menu.add(menuItemNew);
        menu.add(menuItemNewParam);
        menu.add(menuItemExit);
        Dimension d = mainWindowFrame.getSize();
        mainWindowFrame.getContentPane().removeAll();
        mainWindowFrame.setSize(d);
        mainWindowFrame.add(mainPlayGUIPanel);
        mainWindowFrame.pack();
        mainWindowFrame.getContentPane().repaint();
    }
}
