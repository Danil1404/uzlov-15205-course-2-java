package ru.nsu.fit.g15205.uzlov.flow.game;

import ru.nsu.fit.g15205.uzlov.flow.game.core.Cell;
import ru.nsu.fit.g15205.uzlov.flow.game.core.GameField;
import ru.nsu.fit.g15205.uzlov.flow.game.core.MyColor;

import java.awt.*;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by Danil on 28.03.2017.
 */
public class GameFieldStandard implements GameField {
    private final int width;
    private final int height;
    private Cell[][] field;
    private Map<MyColor, Dimension[]> sources = new TreeMap<>();
    GameFieldStandard(int width, int height) {
        if (width < 5 || height < 5)
            throw new ArrayIndexOutOfBoundsException("Can't set field size less than 5.");
        this.width = width;
        this.height = height;
        field = new Cell[height][];
        for (int i = 0; i < height; i++) {
            field[i] = new Cell[width];
            for (int j = 0; j < width; j++)
                field[i][j] = new CellStandard();
        }
    }
    @Override
    public int getWidth() {
        return width;
    }
    @Override
    public int getHeight() {
        return height;
    }
    @Override
    public Dimension[] getSourcePair(MyColor color) {
        return sources.get(color);
    }
    @Override
    public Dimension getSecondSource(Dimension source) {
        Dimension[] d = sources.get(this.getCell(source).getColor());
        if (d.length != 2)
            throw new RuntimeException("Field error: Source count of " + this.getCell(source).getColor() + " != 2");

        if (d[0].width == source.width && d[0].height == source.height)
            return d[1];
        else
            return d[0];
    }
    @Override
    public boolean isOnField(int x, int y) {
        return x >= 0 && x < width && y >= 0 && y < height;
    }
    @Override
    public boolean isOnField(Dimension point) {
        return isOnField(point.width, point.height);
    }
    @Override
    public boolean isPossibleMove(Dimension initialPoint, Dimension movePoint) {
        return isOnField(movePoint) && Math.abs(initialPoint.width - movePoint.width) + Math.abs(initialPoint.height - movePoint.height) == 1;
    }
    @Override
    public boolean isFull() {
        for (int j = 0; j < height; j++) {
            for (int i = 0; i < width; i++) {
                Cell ci = getCell(i, j);
                if (ci.getColor() == null)
                    return false;
                if (ci.isSource() && ci.getIn() == null && ci.getOut() == null)
                    return false;
            }
        }
        return true;
    }
    @Override
    public Cell getCell(Dimension point) {
        return getCell(point.width, point.height);
    }
    @Override
    public Cell getCell(int x, int y) {
        return field[y][x];
    }
    @Override
    public void setSource(MyColor color, Dimension point1, Dimension point2) {
        if (sources.get(color) != null)
            throw new RuntimeException("Нельзя добавить более двух источников одного цвета");
//        if (point1.equals(point2))
//            throw new RuntimeException("Попытка добавить одну точку как два источника");
        field[point1.height][point1.width] = new CellSource(color);
        field[point2.height][point2.width] = new CellSource(color);
        sources.put(color, new Dimension[] { (Dimension)point1.clone(), (Dimension)point2.clone() });
    }
    public void changeSource(MyColor color, Dimension point1, Dimension point2) {
        if (sources.get(color) == null)
            throw new RuntimeException("Нельзя переназначить несуществующий источник");
        Dimension[] md = getSourcePair(color);
        field[md[0].height][md[0].width] = new CellStandard();
        field[md[1].height][md[1].width] = new CellStandard();
        field[point1.height][point1.width] = new CellSource(color);
        field[point2.height][point2.width] = new CellSource(color);
        sources.put(color, new Dimension[] { (Dimension)point1.clone(), (Dimension)point2.clone() });
    }
}
