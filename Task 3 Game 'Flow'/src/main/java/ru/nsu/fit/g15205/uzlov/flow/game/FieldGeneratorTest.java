package ru.nsu.fit.g15205.uzlov.flow.game;

import ru.nsu.fit.g15205.uzlov.flow.game.core.*;

import java.awt.*;


/**
 * Created by Danil on 02.04.2017.
 */
public class FieldGeneratorTest implements FieldGenerator {
    public GameField generateNewField(int width, int height, int colorsCount) {
        GameField gf = new GameFieldStandard(width, height);
        MyColor m[] = MyColor.values();
        Dimension d1 = new Dimension(), d2 = new Dimension();

        d1.height = 0;
        d2.height = height - 1;
        for (int i = 0; i < width; i++) {
            d1.width = i;
            d2.width = i;
            gf.setSource(m[i % m.length], d1, d2);
        }
        return gf;
    }
}
