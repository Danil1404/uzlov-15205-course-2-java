package ru.nsu.fit.g15205.uzlov.flow.game.core;

import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by Danil on 02.04.2017.
 */
public enum Direction {
    UP(0, 0, -1),
    RIGHT(1, 1, 0),
    DOWN(2, 0, 1),
    LEFT(3, -1, 0);
    private final int rightIndex, oppositeIndex, leftIndex;
    public final int dx, dy;
    Direction(int ordinal, int dx, int dy) {
        this.rightIndex = (ordinal + 1) % 4;
        this.oppositeIndex = (ordinal + 2) % 4;
        this.leftIndex = (ordinal + 3) % 4;
        this.dx = dx;
        this.dy = dy;
    }
    public Direction right() {
        return values()[rightIndex];
    }
    public Direction opposite() {
        return values()[oppositeIndex];
    }
    public Direction left() {
        return values()[leftIndex];
    }
    static public Direction randomDirection() {
        return Direction.values()[ThreadLocalRandom.current().nextInt(Direction.values().length)];
    }
    public int getDx() {
        return dx;
    }
    public int getDy() {
        return dy;
    }
}
