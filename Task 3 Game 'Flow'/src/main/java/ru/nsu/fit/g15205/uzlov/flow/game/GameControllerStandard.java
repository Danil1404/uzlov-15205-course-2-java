package ru.nsu.fit.g15205.uzlov.flow.game;

import ru.nsu.fit.g15205.uzlov.flow.game.core.*;

import java.awt.*;

/**
 * Created by Danil on 02.04.2017.
 */
public class GameControllerStandard implements GameController {
    private GameField gf;
    private FieldGenerator fg;
    private Dimension currentCoordinate;
    private boolean isWin;
    //  Если мы только что завершили линию, то мы не хотим совершать лишних ходов
    //  Поэтому тогда я ставлю этот флаг и на следующих ходах проверяю его
    private boolean lockLinked = false;
    public GameControllerStandard(FieldGenerator fieldGenerator) {
        this(fieldGenerator, 10, 10);
    }
    public GameControllerStandard(FieldGenerator fieldGenerator, int width, int height) {
        fg = fieldGenerator;
        nextField(width, height);
    }
    public GameControllerStandard(FieldGenerator fieldGenerator, Dimension d) {
        this(fieldGenerator, d.width, d.height);
    }
    public GameField setField(GameField gameField) {
        this.gf = gameField;
        isWin = false;
        currentCoordinate = null;
        lockLinked = false;
        return gameField;
    }
    public GameField getField() {
        return gf;
    }
    public boolean isWin() {
        return isWin;
    }
    public boolean selectCell(Dimension d) {
        //if (currentCoordinate != null)
        //    throw new RuntimeException("Нельлья управлять двумя цветами одновременно");
        Cell ci = gf.getCell(d);
        if (ci.getColor() == null)
            return false;
        if (ci.isSource())
            cutBranch(gf.getSecondSource(d));
        cutBranch(newCoordinate(d, ci.getOut()));
        currentCoordinate = d;
        lockLinked = false;
        return true;
    }
    public GameField nextField() {
        return nextField(gf.getWidth(), gf.getHeight());
    }
    public GameField nextField(int width, int height) {
        if (width < 3 || height < 3)
            return null;
        return this.setField(fg.generateNewField(width, height, MyColor.values().length));
    }
    public void unselectCell() {
        currentCoordinate = null;
        lockLinked = false;
    }
    public boolean makeMove(Dimension moveCoordinate) {
        //  Если GUI требует невозможного хода, возвращаем false
        if (currentCoordinate == null)
            return false;
        if (!gf.isPossibleMove(currentCoordinate, moveCoordinate))
            return false;
        //  Если мы только что завершили линию и хотим сходить в сторону, возвращаем false
        if (lockLinked) {
            if (gf.getCell(moveCoordinate).getColor() != gf.getCell(currentCoordinate).getColor())
                return false;
            else
                lockLinked = false;
        }
        Cell ci = gf.getCell(currentCoordinate);
        Cell cn = gf.getCell(moveCoordinate);
        //  Нельзя воткнуть в источник неродной цвет
        if (cn.isSource() && cn.getColor() != ci.getColor())
            return false;
        //  Это случай, если мы ведем линию в обратном направлении
        if (ci.getColor() == cn.getColor() && cn.getOut() != null) {
            cutBranch(newCoordinate(moveCoordinate, cn.getOut()));
        } else {    //  Это обычный ход
            //  ОБрезаем все, что торчит из места, куда идем
            cutBranch(newCoordinate(currentCoordinate, ci.getOut()));
            Direction moveDirection = getDirection(currentCoordinate, moveCoordinate);
            //  Собственно, устанавливаем новое состояние после хода
            ci.setOut(moveDirection);
            cutBranch(moveCoordinate);
            cn.setIn(ci.getColor(), moveDirection.opposite());
        }
        currentCoordinate = moveCoordinate;
        //  no comments
        isWin = cn.isSource() && gf.isFull();
        //  Проверяем, не замкнули ли мы только что два источника
        if (cn.isSource() && (cn.getIn() != null || cn.getOut() != null))
            lockLinked = true;
        return true;
    }
    private void cutBranch(Dimension initialPoint) {
        if (initialPoint == null)
            return;
        Cell cn = gf.getCell(initialPoint);
        if (cn.getIn() != null) {
            Dimension prev = newCoordinate(initialPoint, cn.getIn());
            gf.getCell(prev).setOut(null);
        }
        Dimension newCoordinate = initialPoint;
        while (cn.getColor() != null) {
            newCoordinate = newCoordinate(newCoordinate, cn.getOut());
            cn.setIn(null, null);
            if (newCoordinate == null)
                break;
            cn.setOut(null);
            cn = gf.getCell(newCoordinate);
        }
    }
    private static Dimension newCoordinate(Dimension initialPoint, Direction moveDirection) {
        Dimension p = new Dimension(0, 0);
        return newCoordinate(p, initialPoint, moveDirection);
    }
    private static Dimension newCoordinate(Dimension point, Dimension initialPoint, Direction moveDirection) {
        if (moveDirection == null)
            return null;
        point.setSize(initialPoint.width + moveDirection.getDx(), initialPoint.height + moveDirection.getDy());
        return point;
    }
    private static Direction getDirection(Dimension initialPoint, Dimension movePoint) {
        if (initialPoint.width < movePoint.width)
            return Direction.RIGHT;
        if (initialPoint.width > movePoint.width)
            return Direction.LEFT;
        if (initialPoint.height < movePoint.height)
            return Direction.DOWN;
        if (initialPoint.height > movePoint.height)
            return Direction.UP;
        return null;
    }
}
