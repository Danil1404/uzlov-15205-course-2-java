package ru.nsu.fit.g15205.uzlov.threads;

import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by Danil on 2017-06-26.
 */
public class DelayHolder {
    private final AtomicLong time;
    public DelayHolder(long time) {
        this.time = new AtomicLong(time);
    }
    public long getDelay() {
        return time.get();
    }
    public void setDelay(long time) {
        this.time.set(time);
    }
}
