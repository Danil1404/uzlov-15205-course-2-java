package ru.nsu.fit.g15205.uzlov.threads.workers.suppliers;

import ru.nsu.fit.g15205.uzlov.threads.DelayHolder;
import ru.nsu.fit.g15205.uzlov.threads.DetailStore;
import ru.nsu.fit.g15205.uzlov.threads.items.Engine;

/**
 * Created by Danil on 2017-06-04.
 */
public class EngineCreator extends AbstractSupplier<Engine> {
    public EngineCreator(DetailStore<Engine> store, DelayHolder holder) {
        super(holder, store);
    }
    @Override
    Engine newDetail() {
        return new Engine();
    }
}
