package ru.nsu.fit.g15205.uzlov.threads;

import ru.nsu.fit.g15205.uzlov.threads.items.Product;
import ru.nsu.fit.g15205.uzlov.threads.workers.Assembler;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Danil on 2017-06-12.
 */
public class ProductStoreController implements Runnable {
    private final int minVal;
    private final int singleRequestSize;
    private final Object forMonitor;
    private final DetailStore<Product> store;
    private final ExecutorService pool;
    private final Assembler assembler;
    public ProductStoreController(FactoryStores factoryStores, DelayHolder holder, int minVal, int singleRequestSize, Object forMonitor, DetailStore<Product> store, int threadCount) {
        this.minVal = minVal;
        this.singleRequestSize = singleRequestSize;
        this.forMonitor = forMonitor;
        this.store = store;
        this.pool = Executors.newFixedThreadPool(threadCount);
        this.assembler = new Assembler(factoryStores, holder);
    }
    @Override
    public void run() {
        while (true) {
            try {
                if (store.size() < minVal) {
                    for (int i = 0; i < singleRequestSize; i++) {
                        pool.submit(assembler);
                        // request to make product from ThreadPool
                    }
                }
                synchronized (forMonitor) {
                    forMonitor.wait();
                }
            } catch (InterruptedException e) {
                break;
            }
        }
        pool.shutdown();
    }
}
