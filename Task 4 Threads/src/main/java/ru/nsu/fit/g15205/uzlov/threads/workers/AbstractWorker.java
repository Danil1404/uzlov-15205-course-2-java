package ru.nsu.fit.g15205.uzlov.threads.workers;

import ru.nsu.fit.g15205.uzlov.threads.DelayHolder;

/**
 * Created by Danil on 2017-06-26.
 */
public abstract class AbstractWorker implements Runnable {
    private final DelayHolder holder;
    public AbstractWorker(DelayHolder holder) {
        this.holder = holder;
    }
    protected void delay() throws InterruptedException {
        Thread.sleep(holder.getDelay());
    }
}
