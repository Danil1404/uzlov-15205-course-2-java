package ru.nsu.fit.g15205.uzlov.threads.items;

import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by Danil on 2017-06-08.
 */
public class Product implements Detail {
    private final long uid;
    private static final AtomicLong maxUID = new AtomicLong(0);
    private final Accessory accessory;
    private final Case cas;
    private final Engine engine;
    public Product(Accessory accessory, Case cas, Engine engine) {
        this.accessory = accessory;
        this.cas = cas;
        this.engine = engine;
        this.uid = maxUID.getAndIncrement();
    }
    public long getUID() {
        return uid;
    }
    public Accessory getAccessory() {
        return accessory;
    }
    public Case getCas() {
        return cas;
    }
    public Engine getEngine() {
        return engine;
    }
}
