package ru.nsu.fit.g15205.uzlov.threads.items;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by Danil on 2017-06-04.
 */
public class Engine implements Detail {
    private final long uid;
    private static final AtomicLong maxUID = new AtomicLong(0);
    public Engine() {
        this.uid = maxUID.getAndIncrement();
    }
    public long getUID() {
        return uid;
    }
}
