package ru.nsu.fit.g15205.uzlov.threads;

import ru.nsu.fit.g15205.uzlov.threads.items.Accessory;
import ru.nsu.fit.g15205.uzlov.threads.items.Case;
import ru.nsu.fit.g15205.uzlov.threads.items.Engine;
import ru.nsu.fit.g15205.uzlov.threads.items.Product;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Created by Danil on 2017-06-13.
 */
public class FactoryStores {
    public final DetailStore<Accessory> accessoryStore;
    public final DetailStore<Case> caseStore;
    public final DetailStore<Engine> engineStore;
    public final DetailStore<Product> productStore;
    public FactoryStores(DetailStore<Accessory> aStore, DetailStore<Case> cStore, DetailStore<Engine> eStore, DetailStore<Product> pStore) {
        this.accessoryStore = aStore;
        this.caseStore = cStore;
        this.engineStore = eStore;
        this.productStore = pStore;
    }
}
