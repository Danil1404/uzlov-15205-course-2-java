package ru.nsu.fit.g15205.uzlov.threads.workers;

import ru.nsu.fit.g15205.uzlov.threads.DelayHolder;
import ru.nsu.fit.g15205.uzlov.threads.DetailStore;
import ru.nsu.fit.g15205.uzlov.threads.FactoryStores;
import ru.nsu.fit.g15205.uzlov.threads.items.Accessory;
import ru.nsu.fit.g15205.uzlov.threads.items.Case;
import ru.nsu.fit.g15205.uzlov.threads.items.Engine;
import ru.nsu.fit.g15205.uzlov.threads.items.Product;

/**
 * Created by Danil on 2017-06-08.
 */
public class Assembler extends AbstractWorker {
    private final DetailStore<Product> productStore;
    private final DetailStore<Accessory> accessoryStore;
    private final DetailStore<Case> caseStore;
    private final DetailStore<Engine> engineStore;
    public Assembler(FactoryStores fc, DelayHolder holder) {
        super(holder);
        this.productStore = fc.productStore;
        this.accessoryStore = fc.accessoryStore;
        this.caseStore = fc.caseStore;
        this.engineStore = fc.engineStore;
    }
    @Override
    public void run() {
        try {
            Accessory a = accessoryStore.take();
            Case c = caseStore.take();
            Engine e = engineStore.take();
            this.delay();
            productStore.put(new Product(a, c, e));
        } catch (InterruptedException ignored) {}
    }
}
