package ru.nsu.fit.g15205.uzlov.threads.items;

/**
 * Created by Danil on 2017-06-04.
 */
public interface Detail {
    long getUID();
}
