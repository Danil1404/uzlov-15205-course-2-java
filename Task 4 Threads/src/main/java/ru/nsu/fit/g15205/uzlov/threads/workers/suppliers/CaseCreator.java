package ru.nsu.fit.g15205.uzlov.threads.workers.suppliers;

import ru.nsu.fit.g15205.uzlov.threads.DelayHolder;
import ru.nsu.fit.g15205.uzlov.threads.DetailStore;
import ru.nsu.fit.g15205.uzlov.threads.items.Case;

/**
 * Created by Danil on 2017-06-04.
 */
public class CaseCreator extends AbstractSupplier<Case> {
    public CaseCreator(DetailStore<Case> store, DelayHolder holder) {
        super(holder, store);
    }
    @Override
    Case newDetail() {
        return new Case();
    }
}