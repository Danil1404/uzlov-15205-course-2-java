package ru.nsu.fit.g15205.uzlov.threads.workers.suppliers;

import ru.nsu.fit.g15205.uzlov.threads.DelayHolder;
import ru.nsu.fit.g15205.uzlov.threads.DetailStore;
import ru.nsu.fit.g15205.uzlov.threads.items.Detail;
import ru.nsu.fit.g15205.uzlov.threads.workers.AbstractWorker;

/**
 * Created by Danil on 2017-06-26.
 */
public abstract class AbstractSupplier<T extends Detail> extends AbstractWorker implements Runnable {
    private final DetailStore<T> store;
    AbstractSupplier(DelayHolder holder, DetailStore<T> store) {
        super(holder);
        this.store = store;
    }
    public void run() {
        while (true) {
            try {
                this.delay();
                store.put(newDetail());
            } catch (InterruptedException e) {
                break;
            }
        }
    }
    abstract T newDetail();
}
