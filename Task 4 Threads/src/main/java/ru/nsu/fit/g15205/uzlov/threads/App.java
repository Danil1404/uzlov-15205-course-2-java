package ru.nsu.fit.g15205.uzlov.threads;

import ru.nsu.fit.g15205.uzlov.threads.items.*;
import ru.nsu.fit.g15205.uzlov.threads.workers.suppliers.AccessoryCreator;
import ru.nsu.fit.g15205.uzlov.threads.workers.suppliers.CaseCreator;
import ru.nsu.fit.g15205.uzlov.threads.workers.Dealer;
import ru.nsu.fit.g15205.uzlov.threads.workers.suppliers.EngineCreator;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Hello world!
 *
 */
public class App {
    public static void main( String[] args ) {
        ConfigReader cf = null;
        try {
            cf = new ConfigReader(App.class.getClassLoader().getResourceAsStream("properties.txt"));
        } catch (Exception e) {
            System.err.println("cant read config: " + e);
            e.printStackTrace();
            System.exit(1);
        }

        DelayHolder dhAccessories = new DelayHolder(cf.getAccessoryCreatorSleepTime());
        DelayHolder dhCases = new DelayHolder(cf.getCaseCreatorSleepTime());
        DelayHolder dhEngines = new DelayHolder(cf.getEngineCreatorSleepTime());
        DelayHolder dhAssemblers = new DelayHolder(cf.getAssemblerSleepTime());
        DelayHolder dhDealers = new DelayHolder(cf.getDealerSleepTime());

        JFrame frame = new JFrame("Threads");
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));

        JLabel accessoriesLabel = new JLabel("Accessories amount: 0");
        JSlider accessoriesSlider = new JSlider(1, 30, cf.getAccessoryCreatorSleepTime() / 100);
        accessoriesSlider.addChangeListener(e -> dhAccessories.setDelay(accessoriesSlider.getValue() * 100));
        DetailStore<Accessory> accessoriesStore = new DetailStore<>(new ArrayBlockingQueue<>(cf.getAccessoryStoreSize()), (value, isIncremented) -> {
            accessoriesLabel.setText("Accessories amount: " + value);
            accessoriesLabel.repaint();
        });

        JLabel casesLabel = new JLabel("Cases amount: 0");
        JSlider casesSlider = new JSlider(1, 30, cf.getCaseCreatorSleepTime() / 100);
        casesSlider.addChangeListener(e -> dhCases.setDelay(casesSlider.getValue() * 100));
        DetailStore<Case> casesStore = new DetailStore<>(new ArrayBlockingQueue<>(cf.getCaseStoreSize()), (value, isIncremented) -> {
            casesLabel.setText("Cases amount: " + value);
            casesLabel.repaint();
        });

        JLabel enginesLabel = new JLabel("Engines amount: 0");
        JSlider enginesSlider = new JSlider(1, 30, cf.getEngineCreatorSleepTime() / 100);
        enginesSlider.addChangeListener(e -> dhEngines.setDelay(enginesSlider.getValue() * 100));
        DetailStore<Engine> enginesStore = new DetailStore<>(new ArrayBlockingQueue<>(cf.getEngineStoreSize()), (value, isIncremented) -> {
            enginesLabel.setText("Engines amount: " + value);
            enginesLabel.repaint();
        });

        AtomicLong al = new AtomicLong(0);
        JLabel productsLabel = new JLabel("Products amount: 0");
        JSlider productsSlider = new JSlider(1, 30, cf.getAssemblerSleepTime() / 100);
        productsSlider.addChangeListener(e -> dhAssemblers.setDelay(productsSlider.getValue() * 100));
        DetailStore<Product> productsStore = new DetailStore<>(new ArrayBlockingQueue<>(cf.getProductStoreSize()), (value, isIncremented) -> {
            long total;
            if (isIncremented) total = al.incrementAndGet();
            else total = al.get();
            productsLabel.setText("Products amount: " + value + " total: " + total);
            productsLabel.repaint();
        });

        JLabel dealersLabel = new JLabel("Dealers:");
        JSlider dealersSlider = new JSlider(1, 30, cf.getDealerSleepTime() / 100);
        dealersSlider.addChangeListener(e -> dhDealers.setDelay(dealersSlider.getValue() * 100));

        panel.add(accessoriesLabel);
        panel.add(accessoriesSlider);
        panel.add(casesLabel);
        panel.add(casesSlider);
        panel.add(enginesLabel);
        panel.add(enginesSlider);
        panel.add(productsLabel);
        panel.add(productsSlider);
        panel.add(dealersLabel);
        panel.add(dealersSlider);

        frame.add(panel);

        FactoryStores fs = new FactoryStores(accessoriesStore, casesStore, enginesStore, productsStore);
        AccessoryCreator[] acArray = new AccessoryCreator[cf.getAccessoryCreatorsCount()];
        for (int i = 0; i < acArray.length; i++) {
            acArray[i] = new AccessoryCreator(fs.accessoryStore, dhAccessories);
        }
        CaseCreator cc = new CaseCreator(fs.caseStore, dhCases);
        EngineCreator ec = new EngineCreator(fs.engineStore, dhEngines);
        Object forMonitor = new Object();
        ProductStoreController psc = new ProductStoreController(fs, dhAssemblers, cf.getFactoryCriticalCount(), cf.getFactoryRequestSize(), forMonitor, fs.productStore, cf.getFactoryWorkingLinesCount());
        Dealer[] dArray = new Dealer[cf.getDealersCount()];
        for (int i = 0; i < dArray.length; i++) {
            dArray[i] = new Dealer(fs.productStore, dhDealers, forMonitor);
        }

        for (AccessoryCreator ac : acArray) {
            new Thread(ac).start();
        }
        new Thread(cc).start();
        new Thread(ec).start();
        new Thread(psc).start();
        for (Dealer dealer : dArray) {
            new Thread(dealer).start();
        }

        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setMinimumSize(new Dimension(250, 215));
        frame.setVisible(true);
    }
}
