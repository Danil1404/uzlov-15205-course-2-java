package ru.nsu.fit.g15205.uzlov.threads;

/**
 * Created by Danil on 2017-06-13.
 */
public interface GUIChanger {
    void setValue(int value, boolean isIncremented);
}
