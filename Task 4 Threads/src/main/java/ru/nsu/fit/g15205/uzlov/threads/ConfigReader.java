package ru.nsu.fit.g15205.uzlov.threads;

import java.io.InputStream;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

/**
 * Created by Danil on 2017-06-13.
 */
public class ConfigReader {
    private final Map<String, Integer> m;
    public ConfigReader(InputStream inputStream) throws WrongConfigurationException {
        if (inputStream == null) {
            int a;
        }
        Scanner input = new Scanner(inputStream);
        m = new TreeMap<>();
        while (input.hasNextLine()) {
            String s = input.nextLine();
            Scanner sc = new Scanner(s);
            if (!sc.hasNext()) continue;
            String propertyName = sc.next();
            if (!sc.hasNext()) {
                throw new WrongConfigurationException(s);
            }
            Integer value = Integer.parseInt(sc.next());
            if (sc.hasNext()) {
                throw new WrongConfigurationException(s);
            }
            m.put(propertyName, value);
        }
    }

    public int getAccessoryStoreSize() {
        Integer accessoryStoreSize = m.get("accessoryStoreSize");
        if (accessoryStoreSize == null)
            return 100;
        return accessoryStoreSize;
    }

    public int getCaseStoreSize() {
        Integer caseStoreSize = m.get("caseStoreSize");
        if (caseStoreSize == null)
            return 100;
        return caseStoreSize;
    }

    public int getEngineStoreSize() {
        Integer engineStoreSize = m.get("engineStoreSize");
        if (engineStoreSize == null)
            return 100;
        return engineStoreSize;
    }

    public int getProductStoreSize() {
        Integer productStoreSize = m.get("productStoreSize");
        if (productStoreSize == null)
            return 100;
        return productStoreSize;
    }

    public int getAccessoryCreatorsCount() {
        Integer accessoryCreatorsCount = m.get("accessoryCreatorsCount");
        if (accessoryCreatorsCount == null)
            return 100;
        return accessoryCreatorsCount;
    }

    public int getFactoryWorkingLinesCount() {
        Integer factoryWorkingLinesCount = m.get("factoryWorkingLinesCount");
        if (factoryWorkingLinesCount == null)
            return 100;
        return factoryWorkingLinesCount;
    }

    public int getFactoryCriticalCount() {
        Integer factoryCriticalCount = m.get("factoryCriticalCount");
        if (factoryCriticalCount == null)
            return 100;
        return factoryCriticalCount;
    }

    public int getFactoryRequestSize() {
        Integer factoryRequestSize = m.get("factoryRequestSize");
        if (factoryRequestSize == null)
            return 100;
        return factoryRequestSize;
    }

    public int getDealersCount() {
        Integer dealersCount = m.get("dealersCount");
        if (dealersCount == null)
            return 100;
        return dealersCount;
    }

    public int getAccessoryCreatorSleepTime() {
        Integer accessoryCreatorSleepTime = m.get("accessoryCreatorSleepTime");
        if (accessoryCreatorSleepTime == null)
            return 100;
        return accessoryCreatorSleepTime;
    }

    public int getCaseCreatorSleepTime() {
        Integer caseCreatorSleepTime = m.get("caseCreatorSleepTime");
        if (caseCreatorSleepTime == null)
            return 100;
        return caseCreatorSleepTime;
    }

    public int getEngineCreatorSleepTime() {
        Integer engineCreatorSleepTime = m.get("engineCreatorSleepTime");
        if (engineCreatorSleepTime == null)
            return 100;
        return engineCreatorSleepTime;
    }

    public int getAssemblerSleepTime() {
        Integer assemblerSleepTime = m.get("assemblerSleepTime");
        if (assemblerSleepTime == null)
            return 100;
        return assemblerSleepTime;
    }

    public int getDealerSleepTime() {
        Integer dealerSleepTime = m.get("dealerSleepTime");
        if (dealerSleepTime == null)
            return 100;
        return dealerSleepTime;
    }
}
