package ru.nsu.fit.g15205.uzlov.threads.workers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.nsu.fit.g15205.uzlov.threads.DelayHolder;
import ru.nsu.fit.g15205.uzlov.threads.DetailStore;
import ru.nsu.fit.g15205.uzlov.threads.items.Product;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by Danil on 2017-06-08.
 */
public class Dealer extends AbstractWorker{
    private final DetailStore<Product> productStore;
    private final Object forMonitor;
    private final Logger l = LogManager.getLogger();
    private final int dealerNumber;
    private static final AtomicInteger maxNumber = new AtomicInteger(0);
    public Dealer(DetailStore<Product> productStore, DelayHolder holder, Object forMonitor) {
        super(holder);
        this.productStore = productStore;
        this.forMonitor = forMonitor;
        this.dealerNumber = maxNumber.getAndIncrement();
    }
    @Override
    public void run() {
        while (true) {
            try {
                Product p = productStore.take();
                l.info("Dealer {}: Product {} (Case {}, Engine {}, Accessory {})", dealerNumber, p.getUID(), p.getCas().getUID(), p.getEngine().getUID(), p.getAccessory().getUID());
                synchronized (forMonitor) {
                    forMonitor.notify();
                }
                this.delay();
            } catch (InterruptedException e) {
                break;
            }
        }
    }
}
