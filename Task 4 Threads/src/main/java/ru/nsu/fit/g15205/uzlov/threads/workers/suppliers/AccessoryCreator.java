package ru.nsu.fit.g15205.uzlov.threads.workers.suppliers;

import ru.nsu.fit.g15205.uzlov.threads.DelayHolder;
import ru.nsu.fit.g15205.uzlov.threads.DetailStore;
import ru.nsu.fit.g15205.uzlov.threads.items.Accessory;

/**
 * Created by Danil on 2017-06-04.
 */
public class AccessoryCreator extends AbstractSupplier<Accessory> {
    public AccessoryCreator(DetailStore<Accessory> store, DelayHolder holder) {
        super(holder, store);
    }
    @Override
    Accessory newDetail() {
        return new Accessory();
    }
}
