package ru.nsu.fit.g15205.uzlov.threads;

import ru.nsu.fit.g15205.uzlov.threads.items.Detail;

import java.util.concurrent.BlockingQueue;

/**
 * Created by Danil on 2017-06-13.
 */
public class DetailStore<T extends Detail> {
    private final BlockingQueue<T> store;
    private final GUIChanger listener;
    public DetailStore(BlockingQueue<T> store, GUIChanger listener) {
        this.store = store;
        this.listener = listener;
    }
    public void put(T detail) throws InterruptedException {
        store.put(detail);
        listener.setValue(store.size(), true);
    }
    public T take() throws InterruptedException {
        T t = store.take();
        listener.setValue(store.size(), false);
        return t;
    }
    public int size() {
        return store.size();
    }
}
