package ru.nsu.fit.g15205.uzlov;

/**
 * Created by Danil on 18.02.2017.
 */
public interface Parser {
    public String readWord();
}
