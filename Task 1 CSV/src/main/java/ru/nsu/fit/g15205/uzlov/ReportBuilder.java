package ru.nsu.fit.g15205.uzlov;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by Danil on 18.02.2017.
 */
public class ReportBuilder {
    ReportGenerator printer;
    ReportBuilder(ReportGenerator printer) {
        this.printer = printer;
    }
    public void buildReport(Map<String, Counter> dictionary) {
        TreeMap<Integer, ArrayList<String>> sorted = new TreeMap<>(Collections.reverseOrder());
        int total = 0;
        for (Map.Entry<String, Counter> entry : dictionary.entrySet()) {
            int count = entry.getValue().getCount();
            ArrayList<String> al = sorted.get(count);
            if (null == al) {
                al = new ArrayList<>();
                al.add(entry.getKey());
                sorted.put(count, al);
            } else {
                al.add(entry.getKey());
            }
            total += entry.getValue().getCount();
        }
        for (Map.Entry<Integer, ArrayList<String>> entry : sorted.entrySet()) {
            int count = entry.getKey();
            for (String s2 : entry.getValue()) {
                printer.printItem(s2, count, (100.0 * count) / total);
            }
        }
    }
}
