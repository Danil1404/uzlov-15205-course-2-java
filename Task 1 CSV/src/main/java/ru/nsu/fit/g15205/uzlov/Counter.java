package ru.nsu.fit.g15205.uzlov;

/**
 * Created by Danil on 18.02.2017.
 */
public class Counter {
    private int count;
    public int getCount() { return count; }
    public void incCount() { count++; }
}
