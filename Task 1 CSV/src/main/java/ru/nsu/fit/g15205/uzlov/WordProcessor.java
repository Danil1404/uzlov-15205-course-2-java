package ru.nsu.fit.g15205.uzlov;

import java.io.Reader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by Danil on 18.02.2017.
 */
public class WordProcessor {
    private Parser reader;
    private ReportGenerator printer;
    WordProcessor(Parser reader, ReportGenerator reportGenerator) {
        this.reader = reader;
        printer = reportGenerator;
    }
    public void countWords() {
        HashMap<String, Counter> dictionary = new HashMap<>();
        String s = reader.readWord();
        while (s != null) {
            Counter c = dictionary.get(s);
            if (null == c) {
                c = new Counter();
                c.incCount();
                dictionary.put(s, c);
            } else {
                c.incCount();
            }
            s = reader.readWord();
        }
        ReportBuilder rb = new ReportBuilder(printer);
        rb.buildReport(dictionary);
    }
}
