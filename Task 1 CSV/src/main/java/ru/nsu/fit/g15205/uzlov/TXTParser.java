package ru.nsu.fit.g15205.uzlov;

import java.io.*;

/**
 * Created by Danil on 18.02.2017.
 */
public class TXTParser implements Parser {
    private Reader r;
    StringBuilder sb;
    TXTParser(Reader reader) {
        r = reader;
        sb = new StringBuilder();
    }
    public String readWord() {
        sb.setLength(0);
        try {
            char c;
            do {
                c = (char)r.read();
            } while (r.ready() && !Character.isLetterOrDigit(c));

            while (r.ready() && Character.isLetterOrDigit(c)) {
                sb.append(c);
                c = (char)r.read();
            }
        } catch (IOException e) {
            System.err.println("Error while reading file: " + e.getLocalizedMessage());
        }
        if (sb.length() == 0)
            return null;
        else
            return sb.toString();
    }
}
