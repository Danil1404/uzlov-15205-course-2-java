package ru.nsu.fit.g15205.uzlov;

import java.io.IOException;
import java.io.Writer;

/**
 * Created by Danil on 18.02.2017.
 */
public class CSVPrinter implements ReportGenerator {
    private Writer w;
    char lim;
    CSVPrinter(Writer writer, char limiter) {
        w = writer;
        lim = limiter;
    }
    public void printItem(String name, int count, double rate) {
        try {
            w.write(name + lim + count + lim + rate + System.lineSeparator());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
