package ru.nsu.fit.g15205.uzlov;

import java.io.*;

/**
 * Hello world!
 *
 */
public class App  {
    public static void main( String[] args ) {
        Reader r = null;
        Writer w = null;

        if (args.length != 2) {
            System.out.println("Use two arguments for input and output files.");
            System.exit(1);
        }
        try {
            r = new InputStreamReader(new FileInputStream(args[0]));
            try {
                w = new OutputStreamWriter(new FileOutputStream(args[1]));
            } catch (FileNotFoundException e) {
                try {
                    r.close();
                } catch (IOException eClose) {
                    eClose.printStackTrace(System.err);
                }
                System.err.println("File " + args[1] + " not found.");
                return;
            }
        } catch (FileNotFoundException e) {
            System.err.println("File " + args[0] + " not found.");
            return;
        }

        WordProcessor m = new WordProcessor(new TXTParser(r), new CSVPrinter(w, ';'));
        m.countWords();

        try {
            r.close();
        } catch (IOException e) {
            e.printStackTrace(System.err);
        }
        try {
            w.close();
        } catch (IOException e) {
            e.printStackTrace(System.err);
        }

        System.out.println("Ended");
    }
}
