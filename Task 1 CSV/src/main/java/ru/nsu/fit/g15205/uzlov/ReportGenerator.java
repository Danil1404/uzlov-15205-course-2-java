package ru.nsu.fit.g15205.uzlov;

/**
 * Created by Danil on 18.02.2017.
 */
public interface ReportGenerator {
    public void printItem(String name, int count, double rate);
}
