package ru.nsu.fit.g15205.uzlov.calculator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.nsu.fit.g15205.uzlov.calculator.core.Calculator;
import ru.nsu.fit.g15205.uzlov.calculator.core.CommandReader;
import ru.nsu.fit.g15205.uzlov.calculator.core.OperationFactory;
import ru.nsu.fit.g15205.uzlov.calculator.operations.OperationFactoryStandard;
import ru.nsu.fit.g15205.uzlov.calculator.core.WrongConfiguration;

import java.io.*;
import java.util.TreeMap;

import static java.lang.System.exit;

/**
 * Hello world!
 *
 */
public class App {
    private static final Logger l = LogManager.getLogger();
    public static void main( String[] args ) {
        l.debug("Start of program 'Calculator'");
        Reader r = null;
        if (args.length > 0) {
            l.info("Using arg as filename: {}", args[0]);
            try {
                r = new InputStreamReader(new FileInputStream(args[0]));
            } catch (FileNotFoundException e) {
                //System.err.println("Input file not found.");
                l.fatal("Can't open file {} for reading", args[0]);
                exit(1);
            }
        }
        else {
            l.info("Using standard console input");
            r = new InputStreamReader(System.in);
        }
        OperationFactory of = null;
        try {
            of = new OperationFactoryStandard();
        } catch (ClassNotFoundException | WrongConfiguration e) {
            //System.err.println("Mistake in configuration file.");
            //System.err.println(e.getMessage());
            l.fatal("Can't read config file: {}", e);
            exit(1);
        }
        CommandReader comReader = new CommandReaderTXT(r);
        Calculator c = new Calculator(of, comReader, new CalcInfoStandard(new MyCalcStack(), new TreeMap<>()));
        c.calculate();
        //System.out.println();
        //System.out.println("All command processed.");
        l.debug("End of the program");
    }
}
