package ru.nsu.fit.g15205.uzlov.calculator.core;

/**
 * Created by Danil on 19.03.2017.
 */
public abstract class ComputeException extends Exception {
    public ComputeException(String s) {
        super(s);
    }
}
