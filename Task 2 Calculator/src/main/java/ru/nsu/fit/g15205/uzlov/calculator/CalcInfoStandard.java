package ru.nsu.fit.g15205.uzlov.calculator;

import ru.nsu.fit.g15205.uzlov.calculator.core.CalcInfo;
import ru.nsu.fit.g15205.uzlov.calculator.core.CalcStack;

import java.util.Map;

/**
 * Created by Danil on 11.03.2017.
 */
public class CalcInfoStandard implements CalcInfo {
    private CalcStack stack;
    private Map<String, Double> defines;
    public CalcInfoStandard(CalcStack stack, Map<String, Double> defines) {
        this.stack = stack;
        this.defines = defines;
    }
    public CalcStack getStack() {
        return stack;
    }
    public Map<String, Double> getDefines() {
        return defines;
    }
}
