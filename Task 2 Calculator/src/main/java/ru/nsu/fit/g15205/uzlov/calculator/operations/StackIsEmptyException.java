package ru.nsu.fit.g15205.uzlov.calculator.operations;

import ru.nsu.fit.g15205.uzlov.calculator.core.ComputeException;

/**
 * Created by Danil on 19.03.2017.
 */
public class StackIsEmptyException extends ComputeException {
    public StackIsEmptyException() {
        super("Trying to get a number from an empty stack.");
    }
}
