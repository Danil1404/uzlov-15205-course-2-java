package ru.nsu.fit.g15205.uzlov.calculator.core;

/**
 * Created by Danil on 11.03.2017.
 */
public interface CalcOperation {
    Double compute(CalcInfo info, CommandInfo commandInfo) throws ComputeException, WrongArgumentListException;
}
