package ru.nsu.fit.g15205.uzlov.calculator.core;

import java.util.List;

/**
 * Created by Danil on 01.04.2017.
 */
public interface CommandInfo {
    String getCommandName();
    List<String> getArguments();
}
