package ru.nsu.fit.g15205.uzlov.calculator;

import ru.nsu.fit.g15205.uzlov.calculator.core.CommandInfo;

import java.util.List;

/**
 * Created by Danil on 01.04.2017.
 */
public class CommandInfoStandard implements CommandInfo{
    private String name;
    private List<String> arguments;
    public CommandInfoStandard(String name, List<String> arguments) {
        this.name = name;
        this.arguments = arguments;
    }
    public String getCommandName() {
        return name;
    }
    public List<String> getArguments() {
        return arguments;
    }
}
