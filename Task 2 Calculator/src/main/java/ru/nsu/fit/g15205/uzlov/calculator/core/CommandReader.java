package ru.nsu.fit.g15205.uzlov.calculator.core;

import java.util.List;

/**
 * Created by Danil on 11.03.2017.
 */
public interface CommandReader {
    CommandInfo getNextCommand();
}
