package ru.nsu.fit.g15205.uzlov.calculator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.nsu.fit.g15205.uzlov.calculator.core.CalcStack;
import ru.nsu.fit.g15205.uzlov.calculator.operations.StackIsEmptyException;

import java.util.ArrayDeque;
import java.util.Deque;

/**
 * Created by Danil on 19.03.2017.
 */
public class MyCalcStack implements CalcStack {
    private static final Logger l = LogManager.getLogger();
    private Deque<Double> stack;
    public MyCalcStack() {
        stack = new ArrayDeque<>();
    }
    public Double push(Double value) {
        stack.addLast(value);
        return value;
    }
    public Double pop() throws StackIsEmptyException {
        if (stack.size() == 0) {
            l.error("Trying to pop empty stack");
            throw new StackIsEmptyException();
        }
        return stack.pop();
    }
    public Double peek() throws StackIsEmptyException{
        if (stack.size() == 0) {
            l.error("Trying to peek empty stack");
            throw new StackIsEmptyException();
        }
        return stack.peekLast();
    }
    public int size() {
        return stack.size();
    }
    public void clear() {
        stack.clear();
    }
}
