package ru.nsu.fit.g15205.uzlov.calculator.core;

import java.util.List;

/**
 * Created by Danil on 19.03.2017.
 */
public class WrongArgumentListException extends Exception {
    private String name;
    private List<String> args;
    public WrongArgumentListException(String name, List<String> args) {
        this.name = name;
        this.args = args;
    }
    public String getMessage() {
        if (args == null)
            return "Command \"" + name + "\" must have arguments." + System.lineSeparator();
        StringBuilder s = new StringBuilder("Command \"" + name + "\" can not have such list of arguments: ");
        for (String i : args) {
            s.append(i).append(" ");
        }
        s.append(System.lineSeparator());
        return s.toString();
    }
}
