package ru.nsu.fit.g15205.uzlov.calculator.operations;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.nsu.fit.g15205.uzlov.calculator.core.*;

import java.util.List;

/**
 * Created by Danil on 11.03.2017.
 */
public class OperationPop implements CalcOperation {
        private static final Logger l = LogManager.getLogger();
    public Double compute(CalcInfo info, CommandInfo commandInfo) throws WrongArgumentListException, StackIsEmptyException {
        List<String> args = commandInfo.getArguments();
        if (args != null) {
            l.error("Wrong argument list for this command '{}': {}", commandInfo.getCommandName(), args);
            throw new WrongArgumentListException(commandInfo.getCommandName(), args);
        }
        return info.getStack().pop();
    }
}
