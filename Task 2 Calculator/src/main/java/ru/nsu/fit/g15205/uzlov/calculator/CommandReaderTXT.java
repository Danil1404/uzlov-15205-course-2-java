package ru.nsu.fit.g15205.uzlov.calculator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.nsu.fit.g15205.uzlov.calculator.core.CommandInfo;
import ru.nsu.fit.g15205.uzlov.calculator.core.CommandReader;

import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by Danil on 11.03.2017.
 */
public class CommandReaderTXT implements CommandReader {
    private static final Logger l = LogManager.getLogger();
    private Scanner input;
    CommandReaderTXT(Reader input) {
        this.input = new Scanner(input);
    }
    public CommandInfo getNextCommand() {
        String s;
        while(true) {
            if (!input.hasNextLine()) {
                l.debug("Reach end of input");
                return null;
            }
            s = input.nextLine();
            if (s.length() == 0) continue;
            if (s.charAt(0) != '#') break;
        }
        Scanner sc = new Scanner(s);
        List<String> list = null;
        if (!sc.hasNext()) {
            l.debug("Reach end of input");
            return null;
        }
        String name = sc.next();
        if (sc.hasNext()) {
            list = new ArrayList<>();
            while (sc.hasNext()) {
                list.add(sc.next());
            }
        }
        l.debug("Read next command: {} with args: {}", name, list);
        return new CommandInfoStandard(name, list);
    }
}
