package ru.nsu.fit.g15205.uzlov.calculator.operations;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.nsu.fit.g15205.uzlov.calculator.App;
import ru.nsu.fit.g15205.uzlov.calculator.core.*;

import java.util.Map;
import java.util.TreeMap;

/**
 * Created by Danil on 11.03.2017.
 */
public class OperationFactoryStandard implements OperationFactory {
    private static final Logger l = LogManager.getLogger();
    private Map<String, CalcOperation> classes;
    public OperationFactoryStandard() throws ClassNotFoundException, WrongConfiguration {
        Map<String, String> classNames = new ConfigReaderTXT(App.class.getClassLoader().getResourceAsStream("configuration.txt")).getNamesMap();
        classes = new TreeMap<>();
        for (Map.Entry<String, String> entry : classNames.entrySet()) {
            try {
                l.debug("Invoking class {}", entry.getValue());
                classes.put(entry.getKey(), (CalcOperation) Class.forName(entry.getValue()).newInstance());
            } catch (InstantiationException | IllegalAccessException e) {
                //e.printStackTrace();
                l.error("Can't initiate class '{}': {}", entry.getKey(), e);
            }
        }
    }
    public CalcOperation getOperation(String name) throws CommandNotFoundException, WrongArgumentListException {
        CalcOperation op = classes.get(name);
        if (op == null) {
            l.warn("Command '{}' not found", name);
            throw new CommandNotFoundException(name);
        }
        return op;
    }
}
