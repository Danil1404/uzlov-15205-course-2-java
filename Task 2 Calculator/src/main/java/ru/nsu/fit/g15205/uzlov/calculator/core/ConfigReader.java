package ru.nsu.fit.g15205.uzlov.calculator.core;

import java.util.Map;

/**
 * Created by Danil on 12.03.2017.
 */
public interface ConfigReader {
    Map<String, String> getNamesMap() throws WrongConfiguration;
}
