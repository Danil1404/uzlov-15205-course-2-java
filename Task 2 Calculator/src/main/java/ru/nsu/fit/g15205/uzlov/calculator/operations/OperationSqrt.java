package ru.nsu.fit.g15205.uzlov.calculator.operations;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.nsu.fit.g15205.uzlov.calculator.core.*;

import java.util.List;

import static java.lang.StrictMath.sqrt;

/**
 * Created by Danil on 11.03.2017.
 */
public class OperationSqrt implements CalcOperation {
    private static final Logger l = LogManager.getLogger();
    public Double compute(CalcInfo info, CommandInfo commandInfo) throws WrongArgumentListException, RootOfNegativeNumberException, StackIsEmptyException {
        List<String> args = commandInfo.getArguments();
        if (args != null) {
            l.error("Wrong argument list for this command '{}': {}", commandInfo.getCommandName(), args);
            throw new WrongArgumentListException(commandInfo.getCommandName(), args);
        }
        CalcStack stack = info.getStack();
        double a = stack.pop();
        if (a < 0) {
            l.error("Trying to get root of negative value '{}'", a);
            throw new RootOfNegativeNumberException();
        }
        return stack.push(sqrt(a));
    }
}
