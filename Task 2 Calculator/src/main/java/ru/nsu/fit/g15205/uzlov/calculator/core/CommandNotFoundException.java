package ru.nsu.fit.g15205.uzlov.calculator.core;

/**
 * Created by Danil on 19.03.2017.
 */
public class CommandNotFoundException extends Exception {
    public CommandNotFoundException(String name) {
        super("Command \"" + name + "\" not found.");
    }
}
