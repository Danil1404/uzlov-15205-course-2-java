package ru.nsu.fit.g15205.uzlov.calculator.operations;

import ru.nsu.fit.g15205.uzlov.calculator.core.ComputeException;

/**
 * Created by Danil on 19.03.2017.
 */
public class NotANumberOrDefineException extends ComputeException {
    NotANumberOrDefineException(String name) {
        super("Argument \"" + name + "\" is not a number of defined parameter.");
    }
}
