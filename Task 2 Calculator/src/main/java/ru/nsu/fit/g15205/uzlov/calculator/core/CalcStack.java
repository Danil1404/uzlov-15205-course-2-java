package ru.nsu.fit.g15205.uzlov.calculator.core;

import ru.nsu.fit.g15205.uzlov.calculator.operations.StackIsEmptyException;

/**
 * Created by Danil on 19.03.2017.
 */
public interface CalcStack {
    Double push(Double value);
    Double pop() throws StackIsEmptyException;
    Double peek() throws StackIsEmptyException;
    int size();
    void clear();
}
