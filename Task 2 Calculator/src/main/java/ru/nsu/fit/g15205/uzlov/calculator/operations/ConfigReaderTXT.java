package ru.nsu.fit.g15205.uzlov.calculator.operations;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.nsu.fit.g15205.uzlov.calculator.core.ConfigReader;
import ru.nsu.fit.g15205.uzlov.calculator.core.WrongConfiguration;

import java.io.InputStream;
import java.util.*;

/**
 * Created by Danil on 12.03.2017.
 */
public class ConfigReaderTXT implements ConfigReader {
    private static final Logger l = LogManager.getLogger();
    private Scanner input;
    public ConfigReaderTXT(InputStream input) {
        this.input = new Scanner(input);
    }
    public Map<String, String> getNamesMap() throws WrongConfiguration {
        Map<String, String> m = new TreeMap<>();
        l.debug("Building assoc map");
        while (input.hasNextLine()) {
            String s = input.nextLine();
            Scanner sc = new Scanner(s);
            if (!sc.hasNext()) continue;
            String command = sc.next();
            if (!sc.hasNext()) {
                l.error("Can't read config at {}: here should be name of class as second element but it's the end of the line", sc);
                throw new WrongConfiguration(s);
            }
            String className = sc.next();
            if (sc.hasNext()) {
                l.error("Line should contain exactly 2 elements but it has more: {}", sc);
                throw new WrongConfiguration(s);
            }
            l.debug("New assoc: {} - {}", command, className);
            m.put(command, className);
        }
        return m;
    }
}
