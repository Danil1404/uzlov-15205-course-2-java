package ru.nsu.fit.g15205.uzlov.calculator.operations;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.nsu.fit.g15205.uzlov.calculator.core.*;

import java.util.List;
import java.util.Map;

/**
 * Created by Danil on 11.03.2017.
 */
public class OperationPush implements CalcOperation {
    private static final Logger l = LogManager.getLogger();
    public Double compute(CalcInfo info, CommandInfo commandInfo) throws WrongArgumentListException, NotANumberOrDefineException {
        List<String> args = commandInfo.getArguments();
        if (args == null || args.size() != 1) {
            l.error("Wrong argument list for this command '{}': {}", commandInfo.getCommandName(), args);
            throw new WrongArgumentListException(commandInfo.getCommandName(), args);
        }
        Map<String, Double> defines = info.getDefines();
        Double a;
        String arg = args.get(0);
        if (defines.containsKey(arg)) {
            a = defines.get(arg);
        } else try {
            a = Double.parseDouble(arg);
        } catch (NumberFormatException e) {
            l.error("Can't recognize '{}' as valid value", arg);
            throw new NotANumberOrDefineException(arg);
        }
        return info.getStack().push(a);
    }
}
