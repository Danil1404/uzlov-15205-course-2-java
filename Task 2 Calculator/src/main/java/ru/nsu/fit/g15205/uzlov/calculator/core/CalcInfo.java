package ru.nsu.fit.g15205.uzlov.calculator.core;

import java.util.List;
import java.util.Map;
import java.util.Stack;

/**
 * Created by Danil on 11.03.2017.
 */
public interface CalcInfo {
    CalcStack getStack();
    Map<String, Double> getDefines();
}
