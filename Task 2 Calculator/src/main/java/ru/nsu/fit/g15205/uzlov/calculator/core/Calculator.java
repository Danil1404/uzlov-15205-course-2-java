package ru.nsu.fit.g15205.uzlov.calculator.core;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

/**
 * Created by Danil on 11.03.2017.
 */
public class Calculator {
    private static final Logger l = LogManager.getLogger();
    private OperationFactory factory;
    private CommandReader reader;
    private CalcInfo info;
    public Calculator(OperationFactory factory, CommandReader reader, CalcInfo info) {
        //Создали объекст калькулятор
        this.factory = factory;
        this.reader = reader;
        this.info = info;
    }
    public void calculate() {
        //"вызван метод счета"
        CommandInfo ci;
        while ((ci = reader.getNextCommand()) != null) {
            try {
                //считали комманду name, получили объекст ci.getClass().getName(), выполнили комманду
                factory.getOperation(ci.getCommandName()).compute(info, ci);
            } catch (ComputeException | CommandNotFoundException | WrongArgumentListException e) {
                //поймали ошибку e.getMessage();
                //System.err.println(e.getMessage());
                l.error("Calculating error: {}", e.getMessage());
            }
        }
    }
}
