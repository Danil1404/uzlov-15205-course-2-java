package ru.nsu.fit.g15205.uzlov.calculator.operations;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.nsu.fit.g15205.uzlov.calculator.core.*;

import java.util.List;
import java.util.Map;

/**
 * Created by Danil on 11.03.2017.
 */
public class OperationDefine implements CalcOperation {
    private static final Logger l = LogManager.getLogger();
    public Double compute(CalcInfo info, CommandInfo commandInfo) throws NotANumberOrDefineException, DoubleDefineException, WrongArgumentListException {
        List<String> args = commandInfo.getArguments();
        if (args == null || args.size() != 2) {
            //плохие аргументы у дефайна
            l.error("Wrong argument list for this command '{}': {}", commandInfo.getCommandName(), args);
            throw new WrongArgumentListException(commandInfo.getCommandName(), args);
        }
        String constName = args.get(0);
        String constValue = args.get(1);
        Map<String, Double> defines = info.getDefines();
        Double a;
        if (defines.containsKey(constName)) {
            //ошибка - двойной дефайн
            l.error("Double define with name '{}'", constName);
            throw new DoubleDefineException(constName);
        }
        if (defines.containsKey(constValue)) {
            a = defines.get(constValue);
        } else try {
            a = Double.parseDouble(constValue);
        } catch (NumberFormatException e) {
            //ошибка - не смогли распознать число для дефайна
            l.error("Can't recognize '{}' as valid value", constValue);
            throw new NotANumberOrDefineException(constValue);
        }
        defines.put(constName, a);
        return a;
    }
}
