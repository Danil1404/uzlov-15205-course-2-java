package ru.nsu.fit.g15205.uzlov.calculator.core;

/**
 * Created by Danil on 26.03.2017.
 */
public class WrongConfiguration extends Exception {
    public WrongConfiguration(String s) {
        super("String \"" + s + "\" is not a valid configuration string");
    }
}
