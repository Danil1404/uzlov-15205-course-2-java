package ru.nsu.fit.g15205.uzlov.calculator.core;

/**
 * Created by Danil on 11.03.2017.
 */
public interface OperationFactory {
    CalcOperation getOperation(String name) throws CommandNotFoundException, WrongArgumentListException;
}
