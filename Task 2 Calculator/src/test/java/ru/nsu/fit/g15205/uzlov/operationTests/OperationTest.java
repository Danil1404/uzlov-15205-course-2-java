package ru.nsu.fit.g15205.uzlov.operationTests;

import ru.nsu.fit.g15205.uzlov.calculator.CalcInfoStandard;
import ru.nsu.fit.g15205.uzlov.calculator.MyCalcStack;
import ru.nsu.fit.g15205.uzlov.calculator.core.*;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

/**
 * Created by Danil on 01.04.2017.
 */
public abstract class OperationTest {
    protected CalcInfo i = new CalcInfoStandard(new MyCalcStack(), new TreeMap<>());
    protected CalcOperation c;
    protected List<String> l = new ArrayList<>();
    public abstract void clear();
    public abstract void operationTest() throws WrongArgumentListException, ComputeException;
    public abstract void exceptionWAL1Test() throws WrongArgumentListException, ComputeException;
}
