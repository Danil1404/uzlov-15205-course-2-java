package ru.nsu.fit.g15205.uzlov.operationTests;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ru.nsu.fit.g15205.uzlov.calculator.CalcInfoStandard;
import ru.nsu.fit.g15205.uzlov.calculator.CommandInfoStandard;
import ru.nsu.fit.g15205.uzlov.calculator.MyCalcStack;
import ru.nsu.fit.g15205.uzlov.calculator.core.*;
import ru.nsu.fit.g15205.uzlov.calculator.operations.RootOfNegativeNumberException;
import ru.nsu.fit.g15205.uzlov.calculator.operations.OperationSqrt;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

import static org.testng.AssertJUnit.assertEquals;

/**
 * Created by Danil on 26.03.2017.
 */
public class SqrtTest extends OperationTest {
    @BeforeClass
    public void init() {
        c = new OperationSqrt();
    }
    @BeforeMethod
    public void setName() {
        l.add("SQRT");
    }
    @AfterMethod
    public void clear() {
        i.getDefines().clear();
        i.getStack().clear();
        l.clear();
    }
    @Test
    public void operationTest() throws ComputeException, WrongArgumentListException {
        i.getStack().push(81.0);
        CommandInfo ci = new CommandInfoStandard("SQRT", null);
        c.compute(i, ci);
        assertEquals("division error", 9.0, i.getStack().pop());
    }
    @Test (expectedExceptions = WrongArgumentListException.class)
    public void exceptionWAL1Test() throws WrongArgumentListException, ComputeException {
        l.add("q");
        CommandInfo ci = new CommandInfoStandard("SQRT", l);
        c.compute(i, ci);
    }
    @Test (expectedExceptions = RootOfNegativeNumberException.class)
    public void exceptionRONNTest() throws ComputeException, WrongArgumentListException {
        i.getStack().push(-1.0);
        CommandInfo ci = new CommandInfoStandard("SQRT", null);
        c.compute(i, ci);
    }
}
