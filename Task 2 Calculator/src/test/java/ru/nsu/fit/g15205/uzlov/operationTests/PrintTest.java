package ru.nsu.fit.g15205.uzlov.operationTests;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ru.nsu.fit.g15205.uzlov.calculator.CalcInfoStandard;
import ru.nsu.fit.g15205.uzlov.calculator.CommandInfoStandard;
import ru.nsu.fit.g15205.uzlov.calculator.MyCalcStack;
import ru.nsu.fit.g15205.uzlov.calculator.core.*;
import ru.nsu.fit.g15205.uzlov.calculator.operations.OperationPrint;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

import static org.testng.AssertJUnit.assertEquals;

/**
 * Created by Danil on 26.03.2017.
 */
public class PrintTest extends OperationTest {
    @BeforeClass
    public void init() {
        c = new OperationPrint();
    }
    @AfterMethod
    public void clear() {
        i.getDefines().clear();
        i.getStack().clear();
        l.clear();
    }
    @Test
    public void operationTest() throws WrongArgumentListException, ComputeException {
        i.getStack().push(10.0);
        CommandInfo ci = new CommandInfoStandard("PRINT", null);
        assertEquals("printing error", 10.0, c.compute(i, ci));
    }
    @Test (expectedExceptions = WrongArgumentListException.class)
    public void exceptionWAL1Test() throws WrongArgumentListException, ComputeException {
        l.add("q");
        CommandInfo ci = new CommandInfoStandard("PRINT", l);
        c.compute(i, ci);
    }
}
