package ru.nsu.fit.g15205.uzlov.operationTests;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ru.nsu.fit.g15205.uzlov.calculator.CalcInfoStandard;
import ru.nsu.fit.g15205.uzlov.calculator.CommandInfoStandard;
import ru.nsu.fit.g15205.uzlov.calculator.MyCalcStack;
import ru.nsu.fit.g15205.uzlov.calculator.core.*;
import ru.nsu.fit.g15205.uzlov.calculator.operations.NotANumberOrDefineException;
import ru.nsu.fit.g15205.uzlov.calculator.operations.OperationDefine;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

import static org.testng.AssertJUnit.assertEquals;

/**
 * Created by Danil on 26.03.2017.
 */
public class DefineTest extends OperationTest {
    @BeforeClass
    public void init() {
        c = new OperationDefine();
    }
    @BeforeMethod
    public void clear() {
        i.getDefines().clear();
        i.getStack().clear();
        l.clear();
    }
    public void operationTest() throws WrongArgumentListException, ComputeException {
        l.add("q");
        l.add("10");
        CommandInfo ci = new CommandInfoStandard("DEFINE", l);
        c.compute(i, ci);
        assertEquals("define didn't work", 10.0, i.getDefines().get("q"));
    }
    @Test (expectedExceptions = WrongArgumentListException.class)
    public void exceptionWAL1Test() throws WrongArgumentListException, ComputeException {
        CommandInfo ci = new CommandInfoStandard("DEFINE", null);
        c.compute(i,ci);
    }
    @Test (expectedExceptions = WrongArgumentListException.class)
    public void exceptionWAL2Test() throws WrongArgumentListException, ComputeException {
        l.add("q");
        l.add("q");
        l.add("q");
        CommandInfo ci = new CommandInfoStandard("DEFINE", l);
        c.compute(i, ci);
    }
    @Test (expectedExceptions = NotANumberOrDefineException.class)
    public void exceptionNANTest() throws ComputeException, WrongArgumentListException {
        l.add("q");
        l.add("e");
        CommandInfo ci = new CommandInfoStandard("DEFINE", l);
        c.compute(i, ci);
    }
}
