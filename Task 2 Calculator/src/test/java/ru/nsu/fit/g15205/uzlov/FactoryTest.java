package ru.nsu.fit.g15205.uzlov;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ru.nsu.fit.g15205.uzlov.calculator.core.CommandNotFoundException;
import ru.nsu.fit.g15205.uzlov.calculator.core.OperationFactory;
import ru.nsu.fit.g15205.uzlov.calculator.core.WrongArgumentListException;
import ru.nsu.fit.g15205.uzlov.calculator.core.WrongConfiguration;
import ru.nsu.fit.g15205.uzlov.calculator.operations.*;

import static org.testng.AssertJUnit.assertEquals;

/**
 * Created by Danil on 04.04.2017.
 */
public class FactoryTest {
    private OperationFactory of;
    @BeforeClass
    void init() throws WrongConfiguration, ClassNotFoundException {
        of = new OperationFactoryStandard();
    }
    @Test
    void CreationTests() throws WrongArgumentListException, CommandNotFoundException {
        String tmp = "Command haven't been created correctly: ";
        assertEquals(tmp + "DEFINE", true, of.getOperation("DEFINE") instanceof OperationDefine);
        assertEquals(tmp + "/", true, of.getOperation("/") instanceof OperationDivide);
        assertEquals(tmp + "-", true, of.getOperation("-") instanceof OperationMinus);
        assertEquals(tmp + "*", true, of.getOperation("*") instanceof OperationMult);
        assertEquals(tmp + "+", true, of.getOperation("+") instanceof OperationPlus);
        assertEquals(tmp + "POP", true, of.getOperation("POP") instanceof OperationPop);
        assertEquals(tmp + "PRINT", true, of.getOperation("PRINT") instanceof OperationPrint);
        assertEquals(tmp + "PUSH", true, of.getOperation("PUSH") instanceof OperationPush);
        assertEquals(tmp + "SQRT", true, of.getOperation("SQRT") instanceof OperationSqrt);
    }
}
