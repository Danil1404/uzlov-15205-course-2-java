package ru.nsu.fit.g15205.uzlov.operationTests;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ru.nsu.fit.g15205.uzlov.calculator.CalcInfoStandard;
import ru.nsu.fit.g15205.uzlov.calculator.CommandInfoStandard;
import ru.nsu.fit.g15205.uzlov.calculator.MyCalcStack;
import ru.nsu.fit.g15205.uzlov.calculator.core.*;
import ru.nsu.fit.g15205.uzlov.calculator.operations.StackIsEmptyException;
import ru.nsu.fit.g15205.uzlov.calculator.operations.OperationPop;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

import static org.testng.AssertJUnit.assertEquals;

/**
 * Created by Danil on 26.03.2017.
 */
public class PopTest extends OperationTest {
    @BeforeClass
    public void init() {
        c = new OperationPop();
    }
    @BeforeMethod
    public void setName() {
        l.add("POP");
    }
    @AfterMethod
    public void clear() {
        i.getDefines().clear();
        i.getStack().clear();
        l.clear();
    }
    @Test
    public void operationTest() throws WrongArgumentListException, ComputeException {
        i.getStack().push(10.0);
        CommandInfo ci = new CommandInfoStandard("POP", null);
        c.compute(i, ci);
        assertEquals("popping error", 0, i.getStack().size());
    }
    @Test (expectedExceptions = WrongArgumentListException.class)
    public void exceptionWAL1Test() throws WrongArgumentListException, ComputeException {
        l.add("q");
        CommandInfo ci = new CommandInfoStandard("POP", l);
        c.compute(i, ci);
    }
    @Test (expectedExceptions = StackIsEmptyException.class)
    public void exceptionSIETest() throws ComputeException, WrongArgumentListException {
        CommandInfo ci = new CommandInfoStandard("POP", null);
        c.compute(i, ci);
    }
}
